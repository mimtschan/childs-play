package at.qe.sepm.skeleton.tests;

import org.junit.Test;

import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.ChildReferencePersonRelation;
import at.qe.sepm.skeleton.model.Configuration;
import at.qe.sepm.skeleton.model.ConfigurationTimes;
import at.qe.sepm.skeleton.model.DayException;
import at.qe.sepm.skeleton.model.Gender;
import at.qe.sepm.skeleton.model.Holiday;
import at.qe.sepm.skeleton.model.Parent;
import at.qe.sepm.skeleton.model.Person;
import at.qe.sepm.skeleton.model.ReferencePerson;
import at.qe.sepm.skeleton.model.Staff;
import at.qe.sepm.skeleton.model.Task;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.UserRole;
import at.qe.sepm.skeleton.model.Weekday;
import java.sql.Time;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

/**
 * Tests to ensure that each entity's implementation of equals conforms to the
 * contract. See {@linkplain http://www.jqno.nl/equalsverifier/} for more
 * information.
 *
 * This class is part of the skeleton project provided for students of the
 * course "Softwaredevelopment and Project Management" offered by the University
 * of Innsbruck.
 */
public class EqualsImplementationTest {

    @Test
    public void testUserEqualsContract() {
        User user1 = new User();
        user1.setUsername("user1");
        User user2 = new User();
        user2.setUsername("user2");
        Person pers1 = new Parent();
        pers1.setPersonId(1l);
        Person pers2 = new Parent();
        pers2.setPersonId(2l);

        EqualsVerifier.forClass(User.class)
                .withPrefabValues(User.class, user1, user2)
                .withPrefabValues(Person.class, pers1, pers2)
                .suppress(Warning.STRICT_INHERITANCE, Warning.ALL_FIELDS_SHOULD_BE_USED).verify();
    }

    @Test
    public void testUserRoleEqualsContract() {
        EqualsVerifier.forClass(UserRole.class).verify();
    }

    @Test
    public void testWeekdayEqualsContract() {
        EqualsVerifier.forClass(Weekday.class).verify();
    }

    @Test
    public void testGenderEqualsContract() {
        EqualsVerifier.forClass(Gender.class).verify();
    }

    @Test
    public void testPersonEqualsContract() {
        Child child1 = new Child();
        child1.setPersonId(1l);
        Child child2 = new Child();
        child2.setPersonId(2l);
        User user1 = new User();
        user1.setUsername("user1");
        User user2 = new User();
        user2.setUsername("user2");
        Person parent1 = new Parent();
        parent1.setPersonId(1l);
        Person parent2 = new Parent();
        parent2.setPersonId(2l);
        EqualsVerifier.forClass(Person.class)
                .withPrefabValues(Child.class, child1, child2)
                .withPrefabValues(User.class, user1, user2)
                .withPrefabValues(Person.class, parent1, parent2)
                .suppress(Warning.STRICT_INHERITANCE, Warning.ALL_FIELDS_SHOULD_BE_USED).verify();
    }

    @Test
    public void testChildEqualsContract() {
        Child child1 = new Child();
        child1.setPersonId(1l);
        Child child2 = new Child();
        child2.setPersonId(2l);

        User user1 = new User();
        user1.setUsername("user1");
        User user2 = new User();
        user2.setUsername("user2");

        Person parent1 = new Parent();
        parent1.setPersonId(1l);
        Person parent2 = new Parent();
        parent2.setPersonId(2l);

        Task task1 = new Task();
        task1.setTaskId(1l);
        Task task2 = new Task();
        task2.setTaskId(2l);

        ReferencePerson ref1 = new ReferencePerson();
        ref1.setPersonId(1l);
        ReferencePerson ref2 = new ReferencePerson();
        ref2.setPersonId(2l);

        ChildReferencePersonRelation childref1 = new ChildReferencePersonRelation();
        childref1.setRelationId(1l);
        ChildReferencePersonRelation childref2 = new ChildReferencePersonRelation();
        childref2.setRelationId(2l);

        EqualsVerifier.forClass(Child.class)
                .withPrefabValues(Child.class, child1, child2)
                .withPrefabValues(User.class, user1, user2)
                .withPrefabValues(Person.class, parent1, parent2)
                .withPrefabValues(Task.class, task1, task2)
                .withPrefabValues(ReferencePerson.class, ref1, ref2)
                .withPrefabValues(ChildReferencePersonRelation.class, childref1, childref2)
                .suppress(Warning.STRICT_INHERITANCE, Warning.ALL_FIELDS_SHOULD_BE_USED).verify();
    }

    @Test
    public void testParentEqualsContract() {
        Child child1 = new Child();
        child1.setPersonId(1l);
        Child child2 = new Child();
        child2.setPersonId(2l);

        User user1 = new User();
        user1.setUsername("user1");
        User user2 = new User();
        user2.setUsername("user2");

        Person parent1 = new Parent();
        parent1.setPersonId(1l);
        Person parent2 = new Parent();
        parent2.setPersonId(2l);

        Parent parent3 = new Parent();
        parent3.setPersonId(3l);
        Parent parent4 = new Parent();
        parent4.setPersonId(4l);
        EqualsVerifier.forClass(Parent.class)
                .withPrefabValues(Child.class, child1, child2)
                .withPrefabValues(User.class, user1, user2)
                .withPrefabValues(Person.class, parent1, parent2)
                .withPrefabValues(Parent.class, parent3, parent4)
                .suppress(Warning.STRICT_INHERITANCE, Warning.ALL_FIELDS_SHOULD_BE_USED).verify();
    }

    @Test
    public void testStaffEqualsContract() {
        Staff staff1 = new Staff();
        staff1.setPersonId(1l);
        Staff staff2 = new Staff();
        staff2.setPersonId(2l);

        User user1 = new User();
        user1.setUsername("user1");
        User user2 = new User();
        user2.setUsername("user2");

        Person parent1 = new Parent();
        parent1.setPersonId(1l);
        Person parent2 = new Parent();
        parent2.setPersonId(2l);
        EqualsVerifier.forClass(Staff.class)
                .withPrefabValues(Staff.class, staff1, staff2)
                .withPrefabValues(User.class, user1, user2)
                .withPrefabValues(Person.class, parent1, parent2)
                .suppress(Warning.STRICT_INHERITANCE, Warning.ALL_FIELDS_SHOULD_BE_USED).verify();
    }

    @Test
    public void testTaskEqualsContract() {
        Task task1 = new Task();
        task1.setTaskId(1l);
        Task task2 = new Task();
        task2.setTaskId(2l);

        Staff staff1 = new Staff();
        staff1.setPersonId(1l);
        Staff staff2 = new Staff();
        staff2.setPersonId(2l);

        User user1 = new User();
        user1.setUsername("user1");
        User user2 = new User();
        user2.setUsername("user2");

        Person parent1 = new Parent();
        parent1.setPersonId(1l);
        Person parent2 = new Parent();
        parent2.setPersonId(2l);

        Parent parent3 = new Parent();
        parent3.setPersonId(3l);
        Parent parent4 = new Parent();
        parent4.setPersonId(4l);

        Child child1 = new Child();
        child1.setPersonId(1l);
        Child child2 = new Child();
        child2.setPersonId(2l);
        EqualsVerifier.forClass(Task.class)
                .withPrefabValues(Task.class, task1, task2)
                .withPrefabValues(Staff.class, staff1, staff2)
                .withPrefabValues(User.class, user1, user2)
                .withPrefabValues(Person.class, parent1, parent2)
                .withPrefabValues(Parent.class, parent3, parent4)
                .withPrefabValues(Child.class, child1, child2)
                .suppress(Warning.STRICT_INHERITANCE, Warning.ALL_FIELDS_SHOULD_BE_USED).verify();
    }

    @Test
    public void testHolidayEqualsContract() {
        EqualsVerifier.forClass(Holiday.class).suppress(Warning.ALL_FIELDS_SHOULD_BE_USED, Warning.STRICT_INHERITANCE).verify();
    }

    @Test
    public void testDayExceptionEqualsContract() {
        DayException exception1 = new DayException();
        exception1.setExceptionId(1l);
        DayException exception2 = new DayException();
        exception2.setExceptionId(2l);

        Time time1 = new Time(1);
        Time time2 = new Time(2);

        User user1 = new User();
        user1.setUsername("user1");
        User user2 = new User();
        user2.setUsername("user2");

        Person parent1 = new Parent();
        parent1.setPersonId(1l);
        Person parent2 = new Parent();
        parent2.setPersonId(2l);

        Child child1 = new Child();
        child1.setPersonId(1l);
        Child child2 = new Child();
        child2.setPersonId(2l);

        ReferencePerson ref1 = new ReferencePerson();
        ref1.setPersonId(1l);
        ReferencePerson ref2 = new ReferencePerson();
        ref2.setPersonId(2l);

        ChildReferencePersonRelation childref1 = new ChildReferencePersonRelation();
        childref1.setRelationId(1l);
        ChildReferencePersonRelation childref2 = new ChildReferencePersonRelation();
        childref2.setRelationId(2l);

        EqualsVerifier.forClass(DayException.class)
                .withPrefabValues(DayException.class, exception1, exception2)
                .withPrefabValues(Time.class, time1, time2)
                .withPrefabValues(User.class, user1, user2)
                .withPrefabValues(Person.class, parent1, parent2)
                .withPrefabValues(Child.class, child1, child2)
                .withPrefabValues(ReferencePerson.class, ref1, ref2)
                .withPrefabValues(ChildReferencePersonRelation.class, childref1, childref2)
                .suppress(Warning.ALL_FIELDS_SHOULD_BE_USED, Warning.STRICT_INHERITANCE).verify();
    }

    @Test
    public void testChildReferencePersonRElationEqualsContract() {
        User user1 = new User();
        user1.setUsername("user1");
        User user2 = new User();
        user2.setUsername("user2");

        Person parent1 = new Parent();
        parent1.setPersonId(1l);
        Person parent2 = new Parent();
        parent2.setPersonId(2l);

        ChildReferencePersonRelation rel1 = new ChildReferencePersonRelation();
        rel1.setRelationId(1l);
        ChildReferencePersonRelation rel2 = new ChildReferencePersonRelation();
        rel1.setRelationId(2l);

        Child child1 = new Child();
        child1.setPersonId(1l);
        Child child2 = new Child();
        child2.setPersonId(2l);

        ReferencePerson ref1 = new ReferencePerson();
        ref1.setPersonId(1l);
        ReferencePerson ref2 = new ReferencePerson();
        ref2.setPersonId(2l);
        EqualsVerifier.forClass(ChildReferencePersonRelation.class)
                .withPrefabValues(ChildReferencePersonRelation.class, rel1, rel2)
                .withPrefabValues(Child.class, child1, child2)
                .withPrefabValues(Person.class, parent1, parent2)
                .withPrefabValues(User.class, user1, user2)
                .withPrefabValues(ReferencePerson.class, ref1, ref2)
                .suppress(Warning.ALL_FIELDS_SHOULD_BE_USED, Warning.STRICT_INHERITANCE).verify();
    }

    @Test
    public void testConfigurationEqualsContract() {
        Configuration conf1 = new Configuration();
        conf1.setConfigId(1l);
        Configuration conf2 = new Configuration();
        conf1.setConfigId(2l);

        Time time1 = new Time(1);
        Time time2 = new Time(2);

        EqualsVerifier.forClass(Configuration.class)
                .withPrefabValues(Configuration.class, conf1, conf2)
                .withPrefabValues(Time.class, time1, time2)
                .suppress(Warning.INHERITED_DIRECTLY_FROM_OBJECT, Warning.STRICT_INHERITANCE, Warning.ALL_FIELDS_SHOULD_BE_USED).verify();
    }

    @Test
    public void testConfigurationTimesEqualsContract() {
        ConfigurationTimes conf1 = new ConfigurationTimes();
        conf1.setDay(Weekday.FRIDAY);
        ConfigurationTimes conf2 = new ConfigurationTimes();
        conf1.setDay(Weekday.MONDAY);

        Time time1 = new Time(1);
        Time time2 = new Time(2);
        EqualsVerifier.forClass(ConfigurationTimes.class)
                .withPrefabValues(ConfigurationTimes.class, conf1, conf2)
                .withPrefabValues(Time.class, time1, time2)
                .suppress(Warning.ALL_FIELDS_SHOULD_BE_USED, Warning.STRICT_INHERITANCE).verify();
    }

    @Test
    public void testReferencePersonEqualsContract() {
        User user1 = new User();
        user1.setUsername("user1");
        User user2 = new User();
        user2.setUsername("user2");

        Person parent1 = new Parent();
        parent1.setPersonId(1l);
        Person parent2 = new Parent();
        parent2.setPersonId(2l);

        Child child1 = new Child();
        child1.setPersonId(1l);
        Child child2 = new Child();
        child2.setPersonId(2l);

        ReferencePerson ref1 = new ReferencePerson();
        ref1.setPersonId(1l);
        ReferencePerson ref2 = new ReferencePerson();
        ref2.setPersonId(2l);
        EqualsVerifier.forClass(ReferencePerson.class)
                .withPrefabValues(ReferencePerson.class, ref1, ref2)
                .withPrefabValues(User.class, user1, user2)
                .withPrefabValues(Person.class, parent1, parent2)
                .withPrefabValues(Child.class, child1, child2)
                .suppress(Warning.STRICT_INHERITANCE, Warning.ALL_FIELDS_SHOULD_BE_USED).verify();
    }

}
