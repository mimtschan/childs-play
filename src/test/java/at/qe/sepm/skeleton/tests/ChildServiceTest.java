package at.qe.sepm.skeleton.tests;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.Weekday;
import at.qe.sepm.skeleton.services.ChildService;
import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class ChildServiceTest {

    @Autowired
    ChildService childService;

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testGetAll() {
        Assert.assertEquals(5, childService.getAllChildren().size());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testGetAllWeekday() {
        Assert.assertEquals("Childs for Weekday Monday not found correctly", 5, childService.getAllActiveChildrenOnWeekday(Weekday.MONDAY).size());
        Assert.assertEquals("Childs for Weekday Tuesday not found correctly", 1, childService.getAllActiveChildrenOnWeekday(Weekday.TUESDAY).size());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testBirthday() {
        Assert.assertEquals("Age calculated wrong", 7, childService.loadPersonById(6L).getAge(new Date()));
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testDisableEnable() {
        Child child = childService.loadPersonById(6L);
        childService.toggleEnable(child);
        child = childService.savePerson(child);
        Assert.assertFalse("Child was not disabled", child.isEnabled());
        childService.toggleEnable(child);
        child = childService.savePerson(child);
        Assert.assertTrue("Child was not enabled", child.isEnabled());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testCreateChild() {
        Child newChild = childService.createChild();
        Assert.assertNotNull("Child was not initialized correctly", newChild.getParents());
        Assert.assertNotNull("Child was not initialized correctly", newChild.getSiblings());
        Assert.assertNotNull("Child was not initialized correctly", newChild.getSubDates());
        newChild.getSubDates().add(Weekday.MONDAY);
        newChild.setFirstName("testchild");
        newChild.setLastName("testchildname");
        newChild = childService.savePerson(newChild);
        Assert.assertEquals("testchild", newChild.getFirstName());
        Assert.assertEquals("testchildname", newChild.getLastName());
        Assert.assertNotNull("Child got no ID", newChild.getPersonId());
        Assert.assertNotEquals("Child got no ID", Long.valueOf(0L), newChild.getPersonId());
        childService.deletePerson(newChild);
    }

}
