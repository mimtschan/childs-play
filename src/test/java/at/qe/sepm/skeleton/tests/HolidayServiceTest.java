package at.qe.sepm.skeleton.tests;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.Holiday;
import at.qe.sepm.skeleton.services.HolidayService;
import java.util.Calendar;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class HolidayServiceTest {

    @Autowired
    HolidayService holidayService;

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testCreateHoliday() {
        Calendar cal = Calendar.getInstance();
        cal.set(2017, Calendar.JUNE, 16);
        Holiday newHoliday = new Holiday();
        newHoliday.setDate(cal.getTime());
        newHoliday.setDescription("Fronleichnam");
        newHoliday = holidayService.save(newHoliday);
        Holiday loadedHoliday = holidayService.loadSingle(newHoliday.getDate());
        Assert.assertEquals("Assert not created correctly", newHoliday, loadedHoliday);
        Assert.assertTrue("Day is not a Holiday", holidayService.isHoliday(cal.getTime()));
        cal.set(Calendar.DAY_OF_MONTH, 23);
        Assert.assertFalse("Day is a Holiday", holidayService.isHoliday(cal.getTime()));
        holidayService.delete(newHoliday);
        Assert.assertNull("Holiday not deleted properly", holidayService.loadSingle(newHoliday.getDate()));
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testGetAllHolidays() {
        Assert.assertEquals("Not all Holidays read", 2, holidayService.getAllHolidays().size());
    }

}
