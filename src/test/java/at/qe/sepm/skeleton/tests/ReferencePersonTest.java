package at.qe.sepm.skeleton.tests;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.ChildReferencePersonRelation;
import at.qe.sepm.skeleton.model.DayException;
import at.qe.sepm.skeleton.model.Gender;
import at.qe.sepm.skeleton.model.ReferencePerson;
import at.qe.sepm.skeleton.services.ChildRefPersonRelationService;
import at.qe.sepm.skeleton.services.DayExceptionService;

import at.qe.sepm.skeleton.services.ReferencePersonService;
import java.util.ArrayList;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class ReferencePersonTest {

    @Autowired
    ReferencePersonService referencePersonService;

    @Autowired
    ChildRefPersonRelationService childRefPersonRelationService;

    @Autowired
    DayExceptionService dayExceptionService;

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testGetAll() {
        Assert.assertEquals(4, referencePersonService.getAllReferencePersons().size());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testGetActive() {
        Assert.assertEquals(2, referencePersonService.getAllConfirmedReferencePerson().size());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testGetInactive() {
        Assert.assertEquals(2, referencePersonService.getAllUnconfirmedReferencePerson().size());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testCreateAndDeleteReferencePerson() {
        ReferencePerson newReferencePerson = referencePersonService.createReferencePerson();
        newReferencePerson.setFirstName("NewUser1");
        newReferencePerson.setLastName("NewUser1");
        newReferencePerson.setGender(Gender.MALE);
        Assert.assertEquals("Sex is worng set", Gender.MALE, newReferencePerson.getGender());

        ReferencePerson savedReferencePerson = referencePersonService.savePerson(newReferencePerson);
        Assert.assertTrue("Referencperson confirmed when created by StAFF", savedReferencePerson.isEnabled());

        ReferencePerson toDelete = referencePersonService.loadPersonById(savedReferencePerson.getPersonId());
        Assert.assertNotNull("Referenceperson was not saved", toDelete);

        //Deletion
        long deletedId = toDelete.getPersonId();
        referencePersonService.deletePerson(toDelete);
        Assert.assertNull("ReferencePerson was not deleted properly", referencePersonService.loadPersonById(deletedId));
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void addRelationsToReferencePerson() {
        ReferencePerson newReferencePerson = referencePersonService.createReferencePerson();
        ChildReferencePersonRelation childref1 = childRefPersonRelationService.createRelation();
        ChildReferencePersonRelation childref2 = childRefPersonRelationService.createRelation();
        childref1.setRelationId(new Long(1));
        Long childRefid = childref1.getId();

        Assert.assertEquals("check tostring", "at.qe.sepm.skeleton.model.ChildReferencePersonRelation[ id=" + childRefid + " ]", childref1.toString());

        newReferencePerson.setReference(new ArrayList<>());
        newReferencePerson.getReference().add(childref1);
        newReferencePerson.getReference().add(childref2);
        ReferencePerson savedReferencePerson = referencePersonService.savePerson(newReferencePerson);

        Assert.assertEquals("test number of relations", 2, savedReferencePerson.getReference().size());
        referencePersonService.deletePerson(savedReferencePerson);
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void addDayExceptionToReferencePerson() {
        ReferencePerson newReferencePerson = referencePersonService.createReferencePerson();
        DayException dayexception1 = dayExceptionService.createDayException();
        DayException dayexception2 = dayExceptionService.createDayException();
        newReferencePerson.setDayException(new ArrayList<>());
        newReferencePerson.getDayException().add(dayexception1);
        newReferencePerson.getDayException().add(dayexception2);
        ReferencePerson savedReferencePerson = referencePersonService.savePerson(newReferencePerson);

        Assert.assertEquals("test number of exceptions", 2, savedReferencePerson.getDayException().size());
        referencePersonService.deletePerson(savedReferencePerson);
    }

}
