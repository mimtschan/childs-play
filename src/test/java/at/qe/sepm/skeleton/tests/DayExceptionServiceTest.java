package at.qe.sepm.skeleton.tests;

import java.util.Calendar;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.services.DayExceptionService;
import at.qe.sepm.skeleton.model.DayException;
import at.qe.sepm.skeleton.services.ChildService;
import at.qe.sepm.skeleton.services.ReferencePersonService;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class DayExceptionServiceTest {

    @Autowired
    DayExceptionService exceptionService;
    @Autowired
    ChildService childService;
    @Autowired
    ReferencePersonService refPersonService;

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testChildData() {
        Calendar cal = Calendar.getInstance();
        cal.set(2017, Calendar.MAY, 22);
        List<DayException> data = exceptionService.getAllValidForDate(cal.getTime());
        Assert.assertEquals("Exception not all found", 2, data.size());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testCreateAbsendException() {
        Calendar cal = Calendar.getInstance();
        cal.set(2017, Calendar.MAY, 22);
        DayException newDayException = exceptionService.createDayException();
        newDayException.setChild(childService.loadPersonById(6L));
        newDayException.setAbsent(true);
        newDayException.setDate(cal.getTime());
        newDayException = exceptionService.saveDayException(newDayException);
        Assert.assertFalse(newDayException.isConfirmed());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testCreateAlternateGetterException() {
        Calendar cal = Calendar.getInstance();
        cal.set(2017, Calendar.MAY, 22);
        DayException newDayException = exceptionService.createDayException();
        newDayException.setChild(childService.loadPersonById(6L));
        newDayException.setAlternateGetter(refPersonService.loadPersonById(25L));
        newDayException.setDate(cal.getTime());
        newDayException = exceptionService.saveDayException(newDayException);
        Assert.assertFalse(newDayException.isConfirmed());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testCreateOutOfTimeException() {
        Calendar cal = Calendar.getInstance();
        cal.set(2017, Calendar.MAY, 22, 6, 0);
        DayException newDayException = exceptionService.createDayException();
        newDayException.setChild(childService.loadPersonById(6L));
        newDayException.setDate(cal.getTime());
        newDayException.setAlternateBringTime(cal.getTime());
        newDayException = exceptionService.saveDayException(newDayException);
        Assert.assertFalse(newDayException.isConfirmed());
    }

}
