package at.qe.sepm.skeleton.tests;

import java.util.Calendar;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.temporary.Range;
import at.qe.sepm.skeleton.model.Weekday;
import at.qe.sepm.skeleton.services.ConfigurationService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class ConfigurationServiceTest {

    @Autowired
    ConfigurationService configService;

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testGet() {
        Assert.assertNotNull("Config not found", configService.getConfig());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testIsOpenOn() {
        Calendar c = Calendar.getInstance();
        c.set(2017, 3, 10);
        Assert.assertTrue("Monday is not Open", configService.isOpenOn(c.getTime()));
        c.set(2017, 3, 11);
        Assert.assertTrue("Tuesday is not Open", configService.isOpenOn(c.getTime()));
        c.set(2017, 3, 12);
        Assert.assertTrue("Wednesday is not Open", configService.isOpenOn(c.getTime()));
        c.set(2017, 3, 13);
        Assert.assertTrue("Thursday is not Open", configService.isOpenOn(c.getTime()));
        c.set(2017, 3, 14);
        Assert.assertTrue("Friday is not Open", configService.isOpenOn(c.getTime()));
        c.set(2017, 3, 15);
        Assert.assertFalse("Saturday is Open", configService.isOpenOn(c.getTime()));
        c.set(2017, 3, 16);
        Assert.assertFalse("Sunday is Open", configService.isOpenOn(c.getTime()));
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testGetInterval() {
        Calendar c = Calendar.getInstance();
        c.set(2017, 3, 10);
        Range<Date> bring = configService.getBringIntervalForDate(c.getTime());
        Range<Date> get = configService.getGetIntervalForDate(c.getTime());
        Calendar real = Calendar.getInstance();
        real.setTime(bring.getBegin());
        Assert.assertEquals("Bring Start Time not right", 8, real.get(Calendar.HOUR_OF_DAY));
        real.setTime(bring.getEnd());
        Assert.assertEquals("Bring End Time not right", 10, real.get(Calendar.HOUR_OF_DAY));
        real.setTime(get.getBegin());
        Assert.assertEquals("Get Start Time not right", 14, real.get(Calendar.HOUR_OF_DAY));
        real.setTime(get.getEnd());
        Assert.assertEquals("Get End Time not right", 16, real.get(Calendar.HOUR_OF_DAY));
    }

    @Test
    public void testToWeekday() {
        Assert.assertEquals(Calendar.MONDAY, Weekday.MONDAY.toCalendarWeekday());
        Assert.assertEquals(Calendar.TUESDAY, Weekday.TUESDAY.toCalendarWeekday());
        Assert.assertEquals(Calendar.WEDNESDAY, Weekday.WEDNESDAY.toCalendarWeekday());
        Assert.assertEquals(Calendar.THURSDAY, Weekday.THURSDAY.toCalendarWeekday());
        Assert.assertEquals(Calendar.FRIDAY, Weekday.FRIDAY.toCalendarWeekday());
        Assert.assertEquals(Calendar.SATURDAY, Weekday.SATURDAY.toCalendarWeekday());
        Assert.assertEquals(Calendar.SUNDAY, Weekday.SUNDAY.toCalendarWeekday());
    }

    @Test
    public void testFromWeekday() {
        Assert.assertEquals(Weekday.MONDAY, Weekday.fromCalendarWeekday(Calendar.MONDAY));
        Assert.assertEquals(Weekday.TUESDAY, Weekday.fromCalendarWeekday(Calendar.TUESDAY));
        Assert.assertEquals(Weekday.WEDNESDAY, Weekday.fromCalendarWeekday(Calendar.WEDNESDAY));
        Assert.assertEquals(Weekday.THURSDAY, Weekday.fromCalendarWeekday(Calendar.THURSDAY));
        Assert.assertEquals(Weekday.FRIDAY, Weekday.fromCalendarWeekday(Calendar.FRIDAY));
        Assert.assertEquals(Weekday.SATURDAY, Weekday.fromCalendarWeekday(Calendar.SATURDAY));
        Assert.assertEquals(Weekday.SUNDAY, Weekday.fromCalendarWeekday(Calendar.SUNDAY));
    }

    @Test
    public void testFromCalendarDay() {
        Calendar c = Calendar.getInstance();
        c.set(2017, 3, 10);
        Assert.assertEquals(Weekday.MONDAY, Weekday.fromCalendarDay(c.getTime()));
    }

}
