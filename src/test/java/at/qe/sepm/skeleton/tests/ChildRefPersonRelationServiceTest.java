package at.qe.sepm.skeleton.tests;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.ChildReferencePersonRelation;
import at.qe.sepm.skeleton.services.ChildRefPersonRelationService;
import at.qe.sepm.skeleton.services.ChildService;
import at.qe.sepm.skeleton.services.ReferencePersonService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class ChildRefPersonRelationServiceTest {

    @Autowired
    ChildRefPersonRelationService childRefPersonRelService;
    @Autowired
    ChildService childService;
    @Autowired
    ReferencePersonService refPersonService;

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testCreateAndDeletion() {
        int numbersOfReferences = 5;
        Assert.assertEquals("Relations not correctly readed", numbersOfReferences, childRefPersonRelService.getAllRelations().size());
        ChildReferencePersonRelation newRef = childRefPersonRelService.createRelation();
        newRef.setChild(childService.loadPersonById(6L));
        newRef.setReferencePerson(refPersonService.loadPersonById(26L));
        newRef = childRefPersonRelService.saveRelation(newRef);
        Assert.assertEquals("Relation not correctly created", numbersOfReferences + 1, childRefPersonRelService.getAllRelations().size());
        childRefPersonRelService.deleteRelation(childRefPersonRelService.getSingle(newRef.getRelationId()));
        Assert.assertEquals("Relation not correctly deleted", numbersOfReferences, childRefPersonRelService.getAllRelations().size());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"PARENT"})
    public void testChildInformation() {
        Assert.assertEquals("Relations for Child not correctly readed", 1, childRefPersonRelService.getRelationForChild(childService.loadPersonById(6L)).size());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testAllChildInformation() {
        Assert.assertEquals("Relations for all Childs not correctly readed", 5, childRefPersonRelService.getRelationForChildren(childService.getAllActiveChildren()).size());
    }

}
