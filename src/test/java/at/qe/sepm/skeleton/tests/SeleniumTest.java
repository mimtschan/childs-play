package at.qe.sepm.skeleton.tests;

import org.junit.Assert;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author maggo
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
public class SeleniumTest {

    private final WebDriver webDriver = new ChromeDriver();

    @Test
    public void LoginAndCreateTest() throws InterruptedException {
        webDriver.get("localhost:8080");
        webDriver.findElement(By.id("username")).sendKeys("admin");
        webDriver.findElement(By.id("password")).sendKeys("passwd");
        webDriver.findElement(By.id("submit")).click();
        Assert.assertEquals("Testing if on welcome page after login", "http://localhost:8080/secured/welcome.xhtml", webDriver.getCurrentUrl());

        webDriver.get("http://localhost:8080/staff/child_overview.xhtml");
        webDriver.findElement(By.id("userForm:childrenTable:createchild")).click();
        WebDriverWait wait1 = new WebDriverWait(webDriver, 5);
        wait1.until(ExpectedConditions.visibilityOfElementLocated(By.id("userForm:firstname")));
        String firstname = "testchild1";
        String lastname = "lasttestchild1";
        webDriver.findElement(By.id("userForm:firstname")).sendKeys(firstname);
        webDriver.findElement(By.id("userForm:lastname")).sendKeys(lastname, Keys.TAB, Keys.TAB, Keys.TAB, Keys.SPACE, Keys.ENTER,
                Keys.TAB, Keys.TAB, Keys.TAB, Keys.TAB, Keys.TAB, Keys.SPACE, Keys.ENTER,
                Keys.TAB, Keys.TAB, Keys.TAB, Keys.TAB, Keys.TAB, Keys.TAB, Keys.ENTER,
                Keys.TAB, Keys.TAB, Keys.TAB, Keys.TAB, Keys.TAB, Keys.TAB,
                Keys.TAB, Keys.TAB, Keys.TAB, Keys.TAB, Keys.TAB, Keys.TAB, Keys.TAB, Keys.ARROW_DOWN);
        webDriver.findElement(By.id("userForm:save")).click();
        WebDriverWait wait2 = new WebDriverWait(webDriver, 5);
        wait2.until(ExpectedConditions.visibilityOfElementLocated(By.id("userForm:succmsg")));

        Assert.assertEquals("searching succes message", "Das neue Kind " + firstname + " " + lastname + " wurde angelegt.", webDriver.findElement(By.id("userForm:succmsg")).getText());

    }

    @Test
    public void checkUrlsStaff() {
        webDriver.get("localhost:8080");
        webDriver.findElement(By.id("username")).sendKeys("admin");
        webDriver.findElement(By.id("password")).sendKeys("passwd");
        webDriver.findElement(By.id("submit")).click();

        String weburl;
        String bodyText;
        weburl = "http://localhost:8080/staff/child_contacts.xhtml";
        webDriver.get(weburl);
        bodyText = webDriver.findElement(By.tagName("body")).getText();
        Assert.assertFalse("Access denied", bodyText.contains("You do not have access to the requested resource."));

        weburl = "http://localhost:8080/staff/child_overview.xhtml";
        webDriver.get(weburl);
        bodyText = webDriver.findElement(By.tagName("body")).getText();
        Assert.assertFalse("Access denied", bodyText.contains("You do not have access to the requested resource."));

        weburl = "http://localhost:8080/staff/child_referenceperson.xhtml";
        webDriver.get(weburl);
        bodyText = webDriver.findElement(By.tagName("body")).getText();
        Assert.assertFalse("Access denied", bodyText.contains("You do not have access to the requested resource."));

        weburl = "http://localhost:8080/staff/manage_config.xhtml";
        webDriver.get(weburl);
        bodyText = webDriver.findElement(By.tagName("body")).getText();
        Assert.assertFalse("Access denied", bodyText.contains("You do not have access to the requested resource."));

        weburl = "http://localhost:8080/staff/manage_holidays.xhtml";
        webDriver.get(weburl);
        bodyText = webDriver.findElement(By.tagName("body")).getText();
        Assert.assertFalse("Access denied", bodyText.contains("You do not have access to the requested resource."));

        weburl = "http://localhost:8080/staff/manage_parents.xhtml";
        webDriver.get(weburl);
        bodyText = webDriver.findElement(By.tagName("body")).getText();
        Assert.assertFalse("Access denied", bodyText.contains("You do not have access to the requested resource."));

        weburl = "http://localhost:8080/staff/manage_requests.xhtml";
        webDriver.get(weburl);
        bodyText = webDriver.findElement(By.tagName("body")).getText();
        Assert.assertFalse("Access denied", bodyText.contains("You do not have access to the requested resource."));

        weburl = "http://localhost:8080/staff/manage_staff.xhtml";
        webDriver.get(weburl);
        bodyText = webDriver.findElement(By.tagName("body")).getText();
        Assert.assertFalse("Access denied", bodyText.contains("You do not have access to the requested resource."));

        weburl = "http://localhost:8080/staff/planner_list.xhtml";
        webDriver.get(weburl);
        bodyText = webDriver.findElement(By.tagName("body")).getText();
        Assert.assertFalse("Access denied", bodyText.contains("You do not have access to the requested resource."));

        weburl = "http://localhost:8080/staff/planner_table.xhtml";
        webDriver.get(weburl);
        bodyText = webDriver.findElement(By.tagName("body")).getText();
        Assert.assertFalse("Access denied", bodyText.contains("You do not have access to the requested resource."));

        weburl = "http://localhost:8080/staff/task_manage.xhtml";
        webDriver.get(weburl);
        bodyText = webDriver.findElement(By.tagName("body")).getText();
        Assert.assertFalse("Access denied", bodyText.contains("You do not have access to the requested resource."));
    }

    @Test
    public void checkUrlsParents() {
        webDriver.get("localhost:8080");
        webDriver.findElement(By.id("username")).sendKeys("lisa");
        webDriver.findElement(By.id("password")).sendKeys("passwd");
        webDriver.findElement(By.id("submit")).click();

        String weburl;
        String bodyText;
        weburl = "http://localhost:8080/staff/child_contacts.xhtml";
        webDriver.get(weburl);
        bodyText = webDriver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Access available", bodyText.contains("You do not have access to the requested resource."));

        weburl = "http://localhost:8080/staff/child_overview.xhtml";
        webDriver.get(weburl);
        bodyText = webDriver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Access available", bodyText.contains("You do not have access to the requested resource."));

        weburl = "http://localhost:8080/staff/child_referenceperson.xhtml";
        webDriver.get(weburl);
        bodyText = webDriver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Access available", bodyText.contains("You do not have access to the requested resource."));

        weburl = "http://localhost:8080/staff/manage_config.xhtml";
        webDriver.get(weburl);
        bodyText = webDriver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Access available", bodyText.contains("You do not have access to the requested resource."));

        weburl = "http://localhost:8080/staff/manage_holidays.xhtml";
        webDriver.get(weburl);
        bodyText = webDriver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Access available", bodyText.contains("You do not have access to the requested resource."));

        weburl = "http://localhost:8080/staff/manage_parents.xhtml";
        webDriver.get(weburl);
        bodyText = webDriver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Access available", bodyText.contains("You do not have access to the requested resource."));

        weburl = "http://localhost:8080/staff/manage_requests.xhtml";
        webDriver.get(weburl);
        bodyText = webDriver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Access available", bodyText.contains("You do not have access to the requested resource."));

        weburl = "http://localhost:8080/staff/manage_staff.xhtml";
        webDriver.get(weburl);
        bodyText = webDriver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Access available", bodyText.contains("You do not have access to the requested resource."));

        weburl = "http://localhost:8080/staff/planner_list.xhtml";
        webDriver.get(weburl);
        bodyText = webDriver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Access available", bodyText.contains("You do not have access to the requested resource."));

        weburl = "http://localhost:8080/staff/planner_table.xhtml";
        webDriver.get(weburl);
        bodyText = webDriver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Access available", bodyText.contains("You do not have access to the requested resource."));

        weburl = "http://localhost:8080/staff/task_manage.xhtml";
        webDriver.get(weburl);
        bodyText = webDriver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Access available", bodyText.contains("You do not have access to the requested resource."));

        weburl = "http://localhost:8080/parents/child_data.xhtml";
        webDriver.get(weburl);
        bodyText = webDriver.findElement(By.tagName("body")).getText();
        Assert.assertFalse("Access denied", bodyText.contains("You do not have access to the requested resource."));

        weburl = "http://localhost:8080/parents/child_referenceperson.xhtml";
        webDriver.get(weburl);
        bodyText = webDriver.findElement(By.tagName("body")).getText();
        Assert.assertFalse("Access denied", bodyText.contains("You do not have access to the requested resource."));

        weburl = "http://localhost:8080/parents/manage_mydata.xhtml";
        webDriver.get(weburl);
        bodyText = webDriver.findElement(By.tagName("body")).getText();
        Assert.assertFalse("Access denied", bodyText.contains("You do not have access to the requested resource."));

        weburl = "http://localhost:8080/parents/manage_requests.xhtml";
        webDriver.get(weburl);
        bodyText = webDriver.findElement(By.tagName("body")).getText();
        Assert.assertFalse("Access denied", bodyText.contains("You do not have access to the requested resource."));

        weburl = "http://localhost:8080/parents/tasks.xhtml";
        webDriver.get(weburl);
        bodyText = webDriver.findElement(By.tagName("body")).getText();
        Assert.assertFalse("Access denied", bodyText.contains("You do not have access to the requested resource."));
    }

    @After
    public void closeBrowser() {
        webDriver.close();
    }

}
