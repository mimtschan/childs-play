package at.qe.sepm.skeleton.tests;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.Gender;
import at.qe.sepm.skeleton.model.Parent;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.UserRole;
import at.qe.sepm.skeleton.services.ParentService;
import at.qe.sepm.skeleton.services.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class ParentServiceTest {

    @Autowired
    ParentService parentService;
    @Autowired
    UserService userService;

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testGetAll() {
        Assert.assertEquals(5, parentService.getAllParents().size());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testGetActive() {
        Assert.assertEquals(4, parentService.getAllActiveParents().size());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testGetInactive() {
        Assert.assertEquals(1, parentService.getAllInactiveParents().size());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testCreateAndDeleteParent() {
        Parent newParent = parentService.createParent();
        newParent.setFirstName("NewUser1");
        newParent.setLastName("NewUser1");
        User userOfParent = newParent.getUser();
        newParent.setGender(Gender.MALE);
        Assert.assertEquals("Sex is worng set", Gender.MALE, newParent.getGender());

        Assert.assertNotNull("Parent got no User on Creation", userOfParent);
        userOfParent.setUsername("newUser");

        Assert.assertTrue("Parent Init Role false", parentService.hasRole(newParent, UserRole.PARENT));
        Assert.assertTrue("Parent Init Role false", parentService.hasRole(newParent, UserRole.INACTPARENT));
        parentService.inactivateParent(newParent);

        Parent savedParent = parentService.savePerson(newParent);
        Parent toDelete = parentService.loadPersonById(savedParent.getPersonId());

        Assert.assertNotNull("Parent was not saved", toDelete);
        Assert.assertNotNull("User for Parent was not saved", toDelete.getUser());

        Assert.assertFalse("Parent inactivate Role false", parentService.hasRole(toDelete, UserRole.PARENT));
        Assert.assertTrue("Parent inactivate Role false", parentService.hasRole(toDelete, UserRole.INACTPARENT));

        parentService.activateParent(toDelete);
        Assert.assertTrue("Parent activate Role false", parentService.hasRole(toDelete, UserRole.PARENT));
        Assert.assertTrue("Parent activate Role false", parentService.hasRole(toDelete, UserRole.INACTPARENT));

        //Deletion
        long deletedId = toDelete.getPersonId();
        parentService.deletePerson(toDelete);
        Assert.assertNull("Parent was not deleted properly", parentService.loadPersonById(deletedId));
        Assert.assertNull("User for Parent was not deleted properly", userService.loadUser("newUser"));
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testPasswordCheck() {
        Parent parent = parentService.loadPersonById(30L);
        Assert.assertFalse("Passwordcheck on Null correct", parentService.checkPassword(null, parent));
        Assert.assertTrue("Passwordcheck failed", parentService.checkPassword("passwd", parent));
        Assert.assertFalse("Passwordcheck always correct", parentService.checkPassword("hugo", parent));
    }
}
