package at.qe.sepm.skeleton.tests;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.services.PersonService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class PersonServiceTest {

    @Autowired
    PersonService personService;

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testGetAll() {
        Assert.assertEquals(12, personService.getAllPersons().size());
    }

//    @Test
//    @WithMockUser(username = "admin", authorities = {"STAFF"})
//    public void testCreateAndDeleteStaff(){
//    	Staff newStaff = childService.createStaff();
//    	newStaff.setFirstName("NewUser1");
//    	newStaff.setLastName("NewUser1");
//    	User userOfStaff = newStaff.getUser();
//    	Assert.assertNotNull("Parent got no User on Creation",userOfStaff);
//    	userOfStaff.setUsername("newUser");
//
//    	Assert.assertTrue("Parent Init Role false",childService.hasRole(newStaff, UserRole.STAFF));
//    	
//    	Staff savedParent = childService.savePerson(newStaff);
//    	Staff toDelete = childService.loadPersonById(savedParent.getPersonId());
//
//    	Assert.assertNotNull("Parent was not saved",toDelete);
//    	Assert.assertNotNull("User for Parent was not saved",toDelete.getUser());
//    	
//    	//Deletion
//    	long deletedId = toDelete.getPersonId();
//    	childService.deletePerson(toDelete);
//    	Assert.assertNull("Parent was not deleted properly",childService.loadPersonById(deletedId));
//    	Assert.assertNull("User for Parent was not deleted properly",userService.loadUser("newUser"));
//    }
}
