package at.qe.sepm.skeleton.tests;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.Staff;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.UserRole;
import at.qe.sepm.skeleton.services.StaffService;
import at.qe.sepm.skeleton.services.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class StaffServiceTest {

    @Autowired
    StaffService staffService;
    @Autowired
    UserService userService;

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testGetAll() {
        Assert.assertEquals(3, staffService.getAllStaff().size());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testCreateAndDeleteStaff() {
        Staff newStaff = staffService.createStaff();
        newStaff.setFirstName("NewUser1");
        newStaff.setLastName("NewUser1");
        User userOfStaff = newStaff.getUser();
        Assert.assertNotNull("Parent got no User on Creation", userOfStaff);
        userOfStaff.setUsername("newUser");

        Assert.assertTrue("Parent Init Role false", staffService.hasRole(newStaff, UserRole.STAFF));

        Staff savedParent = staffService.savePerson(newStaff);
        Staff toDelete = staffService.loadPersonById(savedParent.getPersonId());

        Assert.assertNotNull("Parent was not saved", toDelete);
        Assert.assertNotNull("User for Parent was not saved", toDelete.getUser());

        //Deletion
        long deletedId = toDelete.getPersonId();
        staffService.deletePerson(toDelete);
        Assert.assertNull("Parent was not deleted properly", staffService.loadPersonById(deletedId));
        Assert.assertNull("User for Parent was not deleted properly", userService.loadUser("newUser"));
    }

}
