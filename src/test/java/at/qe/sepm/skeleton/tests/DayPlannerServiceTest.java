package at.qe.sepm.skeleton.tests;

import java.util.Calendar;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.Weekday;
import at.qe.sepm.skeleton.model.temporary.DayInformation;
import at.qe.sepm.skeleton.services.DayPlannerService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class DayPlannerServiceTest {

    @Autowired
    DayPlannerService plannerService;

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testRange() {
        plannerService.getMonthDayInformation(Calendar.getInstance());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testChildData() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DATE, 1);
        cal.set(Calendar.MONTH, Calendar.MAY);
        cal.set(Calendar.YEAR, 2017);
        Map<Child, DayInformation> data = plannerService.getDayInformation(cal.getTime());
        DayInformation dayInfo = data.get(data.keySet().iterator().next());
        Assert.assertEquals("Start of Antonio is wrong", dayInfo.getStart().getHours(), 8);
        Assert.assertEquals("End of Antonio is wrong", dayInfo.getEnd().getHours(), 16);
        Assert.assertEquals("End of Antonio is wrong", dayInfo.getText(), "anwesend");
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testWeekday() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DATE, 1);
        cal.set(Calendar.MONTH, Calendar.MAY);
        cal.set(Calendar.YEAR, 2017);
        Assert.assertEquals("WeekdayWrong", Weekday.fromCalendar(cal), Weekday.MONDAY);
        cal.set(Calendar.DATE, 2);
        Assert.assertEquals("WeekdayWrong", Weekday.fromCalendar(cal), Weekday.TUESDAY);
        cal.set(Calendar.DATE, 3);
        Assert.assertEquals("WeekdayWrong", Weekday.fromCalendar(cal), Weekday.WEDNESDAY);
        cal.set(Calendar.DATE, 4);
        Assert.assertEquals("WeekdayWrong", Weekday.fromCalendar(cal), Weekday.THURSDAY);
        cal.set(Calendar.DATE, 5);
        Assert.assertEquals("WeekdayWrong", Weekday.fromCalendar(cal), Weekday.FRIDAY);
        cal.set(Calendar.DATE, 6);
        Assert.assertEquals("WeekdayWrong", Weekday.fromCalendar(cal), Weekday.SATURDAY);
        cal.set(Calendar.DATE, 7);
        Assert.assertEquals("WeekdayWrong", Weekday.fromCalendar(cal), Weekday.SUNDAY);
    }

}
