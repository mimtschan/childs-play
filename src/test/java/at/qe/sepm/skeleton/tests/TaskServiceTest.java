package at.qe.sepm.skeleton.tests;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.Task;
import at.qe.sepm.skeleton.services.TaskService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class TaskServiceTest {

    @Autowired
    TaskService taskService;

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testCreateTask() {
        Task newTask = taskService.createTask();
        newTask = taskService.save(newTask);
        Task loadedTask = taskService.loadSingle(newTask.getTaskId());
        Assert.assertEquals(newTask, loadedTask);
    }

//    @Test
//    @WithMockUser(username = "admin", authorities = {"STAFF"})
//    public void testGetInterval(){
//    	
//    }
}
