package at.qe.sepm.skeleton.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.temporary.Confirmation;
import at.qe.sepm.skeleton.services.ChildService;
import at.qe.sepm.skeleton.services.ConfirmationService;
import at.qe.sepm.skeleton.services.DayExceptionService;
import at.qe.sepm.skeleton.services.ReferencePersonService;
import at.qe.sepm.skeleton.services.StaffService;
import java.util.Calendar;
import org.junit.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class ConfirmationServiceTest {

//    @Autowired
//    private ConfirmationDetailController confirmationController;
    @Autowired
    private StaffService staffService;
    @Autowired
    private ConfirmationService confirmationService;
    @Autowired
    private DayExceptionService dayExceptionService;
    @Autowired
    private ReferencePersonService refPersonService;
    @Autowired
    private ChildService childService;

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testGetAll() {
        Assert.assertEquals("Get all not correct",
                confirmationService.getRefPersonConfirmations().size() + confirmationService.getDayExceptionConfirmations().size(),
                confirmationService.getAllConfirmations().size());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testReferencePersonConfirm() {
        long id = 25L;
        Confirmation confirmation = confirmationService.createByRefPerson(refPersonService.loadPersonById(id));
        confirmationService.confirm(confirmation, staffService.loadPersonById(1L));
        Assert.assertTrue("Rerefence Person was not enabled", refPersonService.loadPersonById(id).isEnabled());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testReferencePersonDecline() {
        long id = 27L;
        Confirmation confirmation = confirmationService.createByRefPerson(refPersonService.loadPersonById(id));
        confirmationService.decline(confirmation, staffService.loadPersonById(1L));
        Assert.assertNull("Rerefence Person was not deleted", refPersonService.loadPersonById(id));
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testDayExceptionConfirm() {
        long id = 32L;
        Calendar cal = Calendar.getInstance();
        cal.set(2017, Calendar.MAY, 29);
        Child child = childService.loadPersonById(id);
        Confirmation confirmation = confirmationService.createByException(dayExceptionService.getSingleForChildAndDay(child, cal.getTime()));
        confirmationService.confirm(confirmation, staffService.loadPersonById(1L));
        Assert.assertTrue("DayException was not enabled", dayExceptionService.getSingleForChildAndDay(child, cal.getTime()).isConfirmed());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STAFF"})
    public void testDayExceptionDecline() {
        long id = 6L;
        Calendar cal = Calendar.getInstance();
        cal.set(2017, Calendar.MAY, 22);
        Child child = childService.loadPersonById(id);
        Confirmation confirmation = confirmationService.createByException(dayExceptionService.getSingleForChildAndDay(child, cal.getTime()));
        confirmationService.decline(confirmation, staffService.loadPersonById(1L));
        Assert.assertNull("DayException was not deleted", dayExceptionService.getSingleForChildAndDay(child, cal.getTime()));
    }

}
