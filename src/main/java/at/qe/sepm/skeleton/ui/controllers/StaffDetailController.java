package at.qe.sepm.skeleton.ui.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Staff;
import at.qe.sepm.skeleton.services.StaffService;

/**
 * Controller for the user detail view.
 *
 * This class is part of the skeleton project provided for students of the
 * course "Softwaredevelopment and Project Management" offered by the University
 * of Innsbruck.
 */
@Component
@Scope("view")
public class StaffDetailController extends PersonDetailController<Staff> {

    @Autowired
    private StaffService staffService;

    @Override
    protected StaffService getGenericService() {
        return staffService;
    }

    @Override
    protected Staff createGenericPerson() {
        return staffService.createStaff();
    }

    @Override
    public boolean isValid() {
        return super.isValid()
                && isUsernameValid(person.getUser().getUsername())
                && doCheckAttribute(person.getEmail(), 5);
    }

    @Override
    public void doSavePerson() {
        if (isValid() && person.isNew()) {
            successMsg = "Das neue Personalmitglied " + person.getFirstName() + " " + person.getLastName() + " wurde angelegt.";
        } else if (isValid()) {
            successMsg = "Die Änderungen am Personalmitglied " + person.getFirstName() + " " + person.getLastName() + " wurden gespeichert.";
        }
        super.doSavePerson();
    }
}
