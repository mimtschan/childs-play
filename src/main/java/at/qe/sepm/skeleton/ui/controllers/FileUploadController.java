package at.qe.sepm.skeleton.ui.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.model.UploadedFile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import org.apache.commons.io.FilenameUtils;
import org.primefaces.event.FileUploadEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

@Component
@Scope("view")
public class FileUploadController {

    @Autowired
    private Environment env;

    private UploadedFile file;

    private String uploadpath;

    private boolean uploadDone;

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public void doUpload(FileUploadEvent event) {
        if (event != null) {
            Path folder = Paths.get(env.getProperty("upload.path"));
            file = event.getFile();
            String filename = FilenameUtils.getBaseName(file.getFileName());
            String extension = FilenameUtils.getExtension(file.getFileName());
            Path finalfile = null;
            InputStream input = null;
            try {
                finalfile = Files.createTempFile(folder, extension, filename + "." + extension);
                input = file.getInputstream();
                Files.copy(input, finalfile, StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException ex) {
                Logger.getLogger(FileUploadController.class.getName()).log(Level.SEVERE, null, ex);
                return;
            } finally {
                try {
                    if (input != null) {
                        input.close();
                    }
                } catch (IOException ex) {
                    Logger.getLogger(FileUploadController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            this.setUploadpath("/images/" + finalfile.getFileName().toString());
            this.setUploadDone(true);

        }
    }

    public void reset() {
        this.uploadpath = "";
        this.uploadDone = false;
    }

    /**
     * @return the uploadpath
     */
    public String getUploadpath() {
        return uploadpath;
    }

    /**
     * @param uploadpath the uploadpath to set
     */
    public void setUploadpath(String uploadpath) {
        this.uploadpath = uploadpath;
    }

    /**
     * @return the uploadDone
     */
    public boolean isUploadDone() {
        return uploadDone;
    }

    /**
     * @param uploadDone the uploadDone to set
     */
    public void setUploadDone(boolean uploadDone) {
        this.uploadDone = uploadDone;
    }

}
