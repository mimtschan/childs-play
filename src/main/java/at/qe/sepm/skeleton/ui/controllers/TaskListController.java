package at.qe.sepm.skeleton.ui.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Task;
import at.qe.sepm.skeleton.services.TaskService;

/**
 * Controller for the user list view.
 *
 * This class is part of the skeleton project provided for students of the
 * course "Softwaredevelopment and Project Management" offered by the University
 * of Innsbruck.
 */
@Component
@Scope("view")
public class TaskListController {

    @Autowired
    private TaskService taskService;

    private List<Task> taskList;

    /**
     * @return the taskList
     */
    public List<Task> getTaskList() {
        this.doReloadTaskList();
        return taskList;
    }

    public void doReloadTaskList() {
        taskList = taskService.getAllTasks();
    }

}
