package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Holiday;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.services.HolidayService;

/**
 * Controller for the user list view.
 *
 * This class is part of the skeleton project provided for students of the
 * course "Softwaredevelopment and Project Management" offered by the University
 * of Innsbruck.
 */
@Component
@Scope("view")
public class HolidayListController {

    @Autowired
    private HolidayService holidayService;

    private List<Holiday> holidayList;

    /**
     * @return the taskList
     */
    public List<Holiday> getHolidayList() {
        this.doReloadHolidayList();
        return holidayList;
    }

    public void doReloadHolidayList() {
        holidayList = holidayService.getAllHolidays();
    }

}
