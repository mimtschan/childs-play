package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.ChildReferencePersonRelation;

import at.qe.sepm.skeleton.services.ChildRefPersonRelationService;
import at.qe.sepm.skeleton.services.ParentService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("view")
public class ChildRefPersonRelationshipListController {

    @Autowired
    private ChildRefPersonRelationService childRefPersonRelationService;

    @Autowired
    ParentService parentService;

    private List<ChildReferencePersonRelation> childrefPersonRelationList;
    private List<ChildReferencePersonRelation> filteredchildrefPersonRelationList;

    /**
     * @return the relationList
     */
    public List<ChildReferencePersonRelation> getChildRefPersonRelationList() {
        this.doReloadChildRefPersonRelationList();
        return childrefPersonRelationList;
    }

    public void doReloadChildRefPersonRelationList() {
        childrefPersonRelationList = childRefPersonRelationService.getAllRelations();
    }

    /**
     * @return the filteredchildrefPersonRelationList
     */
    public List<ChildReferencePersonRelation> getFilteredchildrefPersonRelationList() {
        return filteredchildrefPersonRelationList;
    }

    /**
     * @param filteredchildrefPersonRelationList the
     * filteredchildrefPersonRelationList to set
     */
    public void setFilteredchildrefPersonRelationList(List<ChildReferencePersonRelation> filteredchildrefPersonRelationList) {
        this.filteredchildrefPersonRelationList = filteredchildrefPersonRelationList;
    }

}
