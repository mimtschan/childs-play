package at.qe.sepm.skeleton.ui.controllers;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.model.timeline.TimelineEvent;
import org.primefaces.model.timeline.TimelineModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.temporary.DayInformation;
import at.qe.sepm.skeleton.services.DayPlannerService;

@Component
@Scope("view")
public class TimeTablePlannerController {

    @Autowired
    private DayPlannerService dayPlanner;

    private TimelineModel model;
    private Date start;
    private Date end;

    @PostConstruct
    public void init() {
        // set initial start / end dates for the axis of the timeline
        Calendar cal = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        start = cal.getTime();

        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        end = cal.getTime();
        // groups

        Map<Child, List<DayInformation>> data = dayPlanner.getVariableMonthDayInformation(today);

        model = new TimelineModel();
        data.forEach((child, infoList) -> {
            infoList.forEach(info -> {
                model.add(new TimelineEvent(info.getText(), info.getStart(), info.getEnd(), false, child.toString(),
                        info.getColorClass()));
            });
        });

        // create timeline model
        // model = new TimelineModel();
        //
        // for (String name : NAMES) {
        // now = new Date();
        // Date end = new Date(now.getTime() - 12 * 60 * 60 * 1000);
        //
        // for (int i = 0; i < 5; i++) {
        // Date start = new Date(end.getTime() + Math.round(Math.random() * 5) *
        // 60 * 60 * 1000);
        // end = new Date(start.getTime() + Math.round(4 + Math.random() * 5) *
        // 60 * 60 * 1000);
        //
        // long r = Math.round(Math.random() * 2);
        // String availability = (r == 0 ? "Unavailable" : (r == 1 ? "Available"
        // : "Maybe"));
        //
        // // create an event with content, start / end dates, editable flag,
        // group name and custom style class
        // TimelineEvent event = new TimelineEvent(availability, start, end,
        // true, name, availability.toLowerCase());
        // model.add(event);
        // }
        // }
    }

    public TimelineModel getModel() {
        return model;
    }

    public Date getStart() {
        return start;
    }

    public Date getEnd() {
        return end;
    }

}
