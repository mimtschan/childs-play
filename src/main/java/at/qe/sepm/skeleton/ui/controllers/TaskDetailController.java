package at.qe.sepm.skeleton.ui.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Task;
import at.qe.sepm.skeleton.services.TaskService;

/**
 * Controller for the user detail view.
 *
 * This class is part of the skeleton project provided for students of the
 * course "Softwaredevelopment and Project Management" offered by the University
 * of Innsbruck.
 */
@Component
@Scope("view")
public class TaskDetailController {

    @Autowired
    private TaskService taskService;

    /**
     * Attribute to cache the currently displayed person
     */
    protected Task task;
    protected String successMsg;
    protected String successHeader;
    protected boolean success;

    public int sortAlphabetically(String a, String b) {
        return String.CASE_INSENSITIVE_ORDER.compare(a, b);
    }

    protected Task createTask() {
        return taskService.createTask();
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        successMsg = "Bitte prüfen Sie, ob alle Pflichtfelder ausgefüllt sind.";
        successHeader = "Fehler beim Bearbeiten";
        success = false;

        this.task = task;
        doReloadTask();
    }

    public void setNewTask() {
        successMsg = "Fehler beim Anlegen. Bitte prüfen Sie, ob alle Pflichtfelder ausgefüllt sind.";
        successHeader = "Fehler beim Anlegen der Aufgabe";
        success = false;
        this.task = createTask();
    }

    public String getSuccessMsg() {
        return successMsg;
    }

    public String getSuccessHeader() {
        return successHeader;
    }

    public boolean getSuccess() {
        return success;
    }

    public void doReloadTask() {
        if (task != null) {
            task = taskService.loadSingle(task.getId());
        }
    }

    public void doSaveTask() {
        if (doCheckIfValid() && task.isNew()) {
            successMsg = "Die Aufgabe wurde erfolgreich angelegt.";
            successHeader = "Aufgabe angelegt";
            task = taskService.save(task);
            success = true;
        } else if (doCheckIfValid()) {
            successMsg = "Die Änderungen an der Aufgabe wurden gespeichert.";
            successHeader = "Änderungen gespeichert";
            task = taskService.save(task);
            success = true;
        }
    }

    public void doDeleteTask() {
        this.taskService.delete(task);
        task = null;
        success = false;
    }

    public boolean doCheckIfValid() {
        return true;
    }

    public boolean doCheckAttribute(String attribute, int min) {
        return attribute != null;
        //&& attribute.length() > min;
    }

    public void doCompleteTask() {
        taskService.completeTask(task);
        task = taskService.save(task);
    }

    public void doCancel() {
        this.task = null;
    }

}
