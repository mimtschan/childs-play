package at.qe.sepm.skeleton.ui.controllers;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.DayException;
import at.qe.sepm.skeleton.model.Weekday;
import at.qe.sepm.skeleton.services.ChildService;
import at.qe.sepm.skeleton.services.ConfigurationService;
import at.qe.sepm.skeleton.services.DayExceptionService;
import java.util.ArrayList;

@Component
@Scope("view")
public class DailyPlannerController {

    @Autowired
    private ChildService childService;
    @Autowired
    private DayExceptionService dayExceptionService;
    @Autowired
    private ConfigurationService configService;

    private Date enteredDate;
    private List<Child> dayRelevantChildren;
    private List<DayException> dayRelevantExceptions;

    @PostConstruct
    public void init() {
        // fill acutal date as default
        enteredDate = Calendar.getInstance().getTime();
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public List<Child> getDayRelevantChildren() {
        if (dayRelevantChildren == null) {
            reloadDayRelevantChildren();
        }
        return dayRelevantChildren;
    }

    public void setDayRelevantChildren(List<Child> dayRelevantChildren) {
        this.dayRelevantChildren = dayRelevantChildren;
    }

    public List<DayException> getDayRelevantExceptions() {
        if (dayRelevantExceptions == null) {
            reloadDayRelevantExceptions();
        }
        return dayRelevantExceptions;
    }

    public List<Child> getDayRelevantBirthdays() {
        if (dayRelevantChildren == null) {
            reloadDayRelevantChildren();
        }
        List<Child> birthdayChildren = new ArrayList<>();
        dayRelevantChildren.forEach((child) -> {
            if (compareDayAndMonth(this.enteredDate, child.getBirthday())) {
                birthdayChildren.add(child);
            }
        });
        return birthdayChildren;
    }

    public void setDayRelevantExceptions(List<DayException> dayRelevantExceptions) {
        this.dayRelevantExceptions = dayRelevantExceptions;
    }

    public void reloadDayRelevantExceptions() {
        this.dayRelevantExceptions = dayExceptionService.getAllValidForDate(this.enteredDate);
    }

    public void reloadDayRelevantChildren() {
        this.dayRelevantChildren = childService.getAllActiveChildrenOnWeekday(Weekday.fromCalendarDay(this.enteredDate));
    }

    public void reloadAll() {
        reloadDayRelevantChildren();
        reloadDayRelevantExceptions();
    }

    public int getCapacity() {
        return configService.getForDate(this.enteredDate).getChildCapacity();
    }

    private boolean compareDayAndMonth(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);
        return cal1.get(Calendar.DATE) == cal2.get(Calendar.DATE)
                && cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH);
    }

}
