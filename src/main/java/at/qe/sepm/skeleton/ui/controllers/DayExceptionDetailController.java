package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.DayException;
import at.qe.sepm.skeleton.model.Person;
import at.qe.sepm.skeleton.notification.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.services.DayExceptionService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;

/**
 *
 */
@Component
@Scope("view")
public class DayExceptionDetailController {

    @Autowired
    private DayExceptionService dayExceptionService;
    @Autowired
    private SessionInfoBean sessionInfo;
    @Autowired
    private EmailService emailService;

    /**
     * Attribute to cache the currently displayed person
     */
    protected DayException dayException;
    protected String successMsg;
    protected String successHeader;
    protected boolean success;

    public int sortAlphabetically(String a, String b) {
        return String.CASE_INSENSITIVE_ORDER.compare(a, b);
    }

    protected DayException createDayException() {
        return dayExceptionService.createDayException();
    }

    public DayException getDayException() {
        return dayException;
    }

    public void setDayException(DayException dayException) {
        successMsg = "Bitte prüfen Sie, ob alle Pflichtfelder ausgefüllt sind.";
        successHeader = "Fehler beim Bearbeiten";
        success = false;

        this.dayException = dayException;
        doReloadDayException();
    }

    public void setNewDayException() {
        successMsg = "Fehler beim Anlegen. Bitte prüfen Sie, ob alle Pflichtfelder ausgefüllt sind.";
        successHeader = "Fehler beim Anlegen";
        success = false;
        this.dayException = createDayException();
    }

    public String getSuccessMsg() {
        return successMsg;
    }

    public String getSuccessHeader() {
        return successHeader;
    }

    public boolean getSuccess() {
        return success;
    }

    public void doReloadDayException() {
        if (dayException != null) {
            dayException = dayExceptionService.getSingle(dayException.getId());
        }
    }

    public void doSaveDayException() {
        if (doCheckIfValid() && dayException.isNew()) {
            successMsg = "Anfrage erfolgreich angelegt";
            successHeader = "Angelegt";
            dayException = dayExceptionService.saveDayException(dayException);
            success = true;
        } else if (doCheckIfValid()) {
            successMsg = "Änderungen an der Anfrage gespeichert";
            successHeader = "Gespeichert";
            dayException = dayExceptionService.saveDayException(dayException);
            success = true;
        }
    }

    public void doDelete() {
        dayExceptionService.deleteException(dayException);
        this.dayException = null;
    }

    public void doDecline(Person decliner) {
        emailService.sendSimpleMessage(dayException, decliner);
        doDelete();
    }

    public void doCancel() {
        dayException = null;
    }

    public void doConfirm() {
        this.dayException = dayExceptionService.confirmException(dayException, sessionInfo.getCurrentUser().getPerson());
    }

    public boolean doCheckIfValid() {
        return true;
    }

    public boolean doCheckAttribute(String attribute, int min) {
        return attribute != null;
        //&& attribute.length() > min;
    }

}
