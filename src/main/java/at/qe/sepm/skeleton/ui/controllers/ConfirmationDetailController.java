package at.qe.sepm.skeleton.ui.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.temporary.Confirmation;
import at.qe.sepm.skeleton.services.ConfirmationService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;

/**
 * Controller for the user detail view.
 *
 * This class is part of the skeleton project provided for students of the
 * course "Softwaredevelopment and Project Management" offered by the University
 * of Innsbruck.
 */
@Component
@Scope("view")
public class ConfirmationDetailController {

    @Autowired
    private ConfirmationService confirmationService;
    @Autowired
    private SessionInfoBean sessionInfo;

    private Confirmation confirmation;

    public Confirmation getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(Confirmation confirmation) {
        this.confirmation = confirmation;
    }

    public void confirmConfirmation() {
        confirmationService.confirm(this.confirmation, sessionInfo.getCurrentUser().getPerson());
    }

    public void declineConfirmation() {
        confirmationService.decline(this.confirmation, sessionInfo.getCurrentUser().getPerson());
    }
}
