package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Holiday;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.services.HolidayService;

/**
 * Controller for the user detail view.
 *
 * This class is part of the skeleton project provided for students of the
 * course "Softwaredevelopment and Project Management" offered by the University
 * of Innsbruck.
 */
@Component
@Scope("view")
public class HolidayDetailController {

    @Autowired
    private HolidayService holidayService;

    /**
     * Attribute to cache the currently displayed person
     */
    protected Holiday holiday;
    protected String successMsg;
    protected String successHeader;
    protected boolean success;

    protected Holiday createHoliday() {
        return new Holiday();
    }

    public Holiday getHoliday() {
        return holiday;
    }

    public void setHoliday(Holiday holiday) {
        successMsg = "Bitte prüfen Sie, ob alle Pflichtfelder ausgefüllt sind.";
        successHeader = "Fehler beim Bearbeiten";
        success = false;

        this.holiday = holiday;
        doReloadHoliday();
    }

    public void setNewHoliday() {
        successMsg = "Fehler beim Anlegen. Bitte prüfen Sie, ob alle Pflichtfelder ausgefüllt sind.";
        successHeader = "Fehler beim Anlegen des Feiertags";
        success = false;
        this.holiday = createHoliday();
    }

    public String getSuccessMsg() {
        return successMsg;
    }

    public String getSuccessHeader() {
        return successHeader;
    }

    public boolean getSuccess() {
        return success;
    }

    public void doReloadHoliday() {
        if (holiday != null && !holiday.isNew()) {
            holiday = holidayService.loadSingle(holiday.getId());
        }
    }

    public void doSaveHoliday() {
        if (doCheckIfValid() && holiday.isNew()) {
            successMsg = successHeader = "Feiertag erfolgreich angelegt";
            holiday = holidayService.save(holiday);
            success = true;
        } else if (doCheckIfValid()) {
            successMsg = successHeader = "Änderungen gespeichert";
            holiday = holidayService.save(holiday);
            success = true;
        }
    }

    public void doDeleteHoliday() {
        this.holidayService.delete(holiday);
        holiday = null;
        success = false;
    }

    public boolean doCheckIfValid() {
        return true;
    }

    public boolean doCheckAttribute(String attribute, int min) {
        return attribute != null;
        //&& attribute.length() > min;
    }

    public void doCancel() {
        holiday = null;
    }

}
