package at.qe.sepm.skeleton.ui.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.ConfigurationTimes;
import at.qe.sepm.skeleton.model.Parent;
import at.qe.sepm.skeleton.model.Weekday;
import at.qe.sepm.skeleton.services.ChildService;
import at.qe.sepm.skeleton.services.ConfigurationService;
import at.qe.sepm.skeleton.services.ParentService;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.event.TransferEvent;

import org.primefaces.model.DualListModel;

/**
 * Controller for the user detail view.
 *
 * This class is part of the skeleton project provided for students of the
 * course "Softwaredevelopment and Project Management" offered by the University
 * of Innsbruck.
 */
@Component
@Scope("view")
public class ChildDetailController extends PersonDetailController<Child> {

    @Autowired
    private ChildService childService;
    @Autowired
    private ParentService parentService;
    @Autowired
    private ConfigurationService configService;

    private DualListModel<Parent> parents;
    private DualListModel<Child> siblings;
    private DualListModel<Weekday> subDates;

    @Override
    protected ChildService getGenericService() {
        return childService;
    }

    @Override
    protected Child createGenericPerson() {
        return childService.createChild();
    }

    /**
     * @return the parents
     */
    public DualListModel<Parent> getParents() {
        parents = new DualListModel<>(parentService.getAllParents(), this.person.getParents());
        //remove actuale parents from sourcelist
        parents.getSource().removeAll(this.person.getParents());

        return parents;
    }

    /**
     * @param parents the parents to set
     */
    public void setParents(DualListModel<Parent> parents) {
        this.parents = parents;
        this.person.setParents(parents.getTarget());
    }

    /**
     * @return the siblings
     */
    public DualListModel<Child> getSiblings() {
        //if (siblings == null) {
        siblings = new DualListModel<>(childService.getAllChildren(), this.person.getSiblings());
        //remove actual siblings from source
        siblings.getSource().removeAll(this.person.getSiblings());
        //remove itself from source
        siblings.getSource().remove(this.person);
        //}
        return siblings;
    }

    /**
     * @param siblings the siblings to set
     */
    public void setSiblings(DualListModel<Child> siblings) {
        this.siblings = siblings;
        this.person.setSiblings(siblings.getTarget());
    }

    public DualListModel<Weekday> getSubdates() {
        List<Weekday> source = new ArrayList<>(configService.getOpenTimeConfigs().keySet());
        List<Weekday> target = new ArrayList<>(this.person.getSubDates());
        subDates = new DualListModel<>(source, target);
        //remove actuale assigned weekdays from sourcelist
        subDates.getSource().removeAll(this.person.getSubDates());

        return subDates;
    }

    /**
     * @param subdates set subdates to set
     */
    public void setSubdates(DualListModel<Weekday> subdates) {
        this.subDates = subdates;
        this.person.setSubDates(new HashSet<>(subdates.getTarget()));
    }

    @Override
    public boolean isValid() {
        return super.isValid()
                && person.getBirthday() != null
                && person.getEmergencyContact() != null
                && person.getParents() != null
                && person.getRegistrationDate() != null
                && person.getDeRegistrationDate() != null;
    }

    @Override
    public void doSavePerson() {

        if (isValid() && person.isNew()) {
            successMsg = "Das neue Kind " + person.getFirstName() + " " + person.getLastName() + " wurde angelegt.";
        } else if (isValid()) {
            successMsg = "Die Änderungen am Kind " + person.getFirstName() + " " + person.getLastName() + " wurden gespeichert.";
        }
        super.doSavePerson();
        //if correct reset siblings list - because then the new child 
        //will appear in the list immediatly
        if (success) {
            siblings = null;
        }
    }

    public void doToggleEnablePerson() {
        childService.toggleEnable(person);
        doSavePerson();
    }

    public void onSubdateTransfer(TransferEvent event) {
        if (event.isAdd()) {
            for (Object item : event.getItems()) {
                ConfigurationTimes weekdayConfig = configService.getForWeekday((Weekday) item);

                int weekdayActCap = childService.getActualCapacityForWeekday(weekdayConfig.getDay()) + 1;
                int weekdayMaxCap = weekdayConfig.getChildCapacity();

                FacesMessage msg = new FacesMessage();
                if (weekdayActCap <= weekdayMaxCap) {
                    msg.setSeverity(FacesMessage.SEVERITY_INFO);
                    msg.setSummary("Kapazität");
                } else {
                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                    msg.setSummary("Kapazität überschritten");
                }

                msg.setDetail("aktuelle Kapazität für " + weekdayConfig.getDay().toString() + ": " + weekdayActCap
                        + "\nMaximum: " + weekdayMaxCap);
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
        }
    }

}
