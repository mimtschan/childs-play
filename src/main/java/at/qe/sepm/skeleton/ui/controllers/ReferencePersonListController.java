package at.qe.sepm.skeleton.ui.controllers;

import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.ReferencePerson;
import at.qe.sepm.skeleton.services.ReferencePersonService;

/**
 * Controller for the user list view.
 *
 * This class is part of the skeleton project provided for students of the
 * course "Softwaredevelopment and Project Management" offered by the University
 * of Innsbruck.
 */
@Component
@Scope("view")
public class ReferencePersonListController extends PersonListController<ReferencePerson> {

    @Autowired
    private ReferencePersonService referencePersonService;

    @Override
    public Map<String, Integer> getFilters() {
        Map<String, Integer> filters = new TreeMap<>();
        filters.put("Nur nicht bestätigte Bezugspersonen", FILT_ACT);
        filters.put("Nur bestätige Bezugspersonen", FILT_INACT);
        filters.put("Alle Bezugspersonen", FILT_ALL);
        return filters;
    }

    @Override
    public void doReloadPersonList() {
        if (filter == FILT_ACT) {
            personList = referencePersonService.getAllUnconfirmedReferencePerson();
        } else if (filter == FILT_INACT) {
            personList = referencePersonService.getAllConfirmedReferencePerson();
        } else {
            personList = referencePersonService.getAllReferencePersons();
        }
    }

    @Override
    protected ReferencePersonService getGenericService() {
        return referencePersonService;
    }

}
