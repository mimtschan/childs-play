package at.qe.sepm.skeleton.ui.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Staff;
import at.qe.sepm.skeleton.services.StaffService;

/**
 * Controller for the user list view.
 *
 * This class is part of the skeleton project provided for students of the
 * course "Softwaredevelopment and Project Management" offered by the University
 * of Innsbruck.
 */
@Component
@Scope("view")
public class StaffListController extends PersonListController<Staff> {

    @Autowired
    private StaffService staffService;

    @Override
    public void doReloadPersonList() {
        personList = staffService.getAllStaff();
    }

    @Override
    protected StaffService getGenericService() {
        return staffService;
    }

}
