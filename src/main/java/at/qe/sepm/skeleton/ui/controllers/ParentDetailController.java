package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Child;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Parent;
import at.qe.sepm.skeleton.services.ChildService;
import at.qe.sepm.skeleton.services.ParentService;
import org.primefaces.model.DualListModel;

/**
 * Controller for the user detail view.
 *
 * This class is part of the skeleton project provided for students of the
 * course "Softwaredevelopment and Project Management" offered by the University
 * of Innsbruck.
 */
@Component
@Scope("view")
public class ParentDetailController extends PersonDetailController<Parent> {

    @Autowired
    private ParentService parentService;
    @Autowired
    private ChildService childService;

    private String passwortOld;
    private DualListModel<Child> children;

    @Override
    protected ParentService getGenericService() {
        return parentService;
    }

    public String getPasswortOld() {
        return passwortOld;
    }

    public void setPasswortOld(String passwortOld) {
        this.passwortOld = passwortOld;
    }

    @Override
    protected Parent createGenericPerson() {
        return parentService.createParent();
    }

    public DualListModel<Child> getChildren() {
        if (children == null) {
            children = new DualListModel<>(childService.getAllChildren(), this.person.getChildren());
            children.getSource().removeAll(this.person.getChildren());
        }
        return children;
    }

    /**
     * @param children the children to set
     */
    public void setChildren(DualListModel<Child> children) {
        this.children = children;
        this.person.setChildren(children.getTarget());
    }

    @Override
    public boolean isValid() {
        return super.isValid()
                && isUsernameValid(person.getUser().getUsername())
                && doCheckAttribute(person.getEmail(), 5)
                && person.getPhonePrivate() != null
                && doCheckAttribute(person.getStreet(), 3)
                && doCheckAttribute(person.getCity(), 1)
                && doCheckAttribute(person.getZipcode(), 3);
    }

    @Override
    public void doSavePerson() {
        if (isValid() && person.isNew()) {
            successMsg = "Der/die neue Erziehungsberechtigte " + person + " wurde angelegt.";
        } else if (isValid()) {
            successMsg = "Die Änderungen an dem/der Erziehungsberechtigten " + person + " wurden gespeichert.";
        }
        super.doSavePerson();
    }

    public void doSaveNewPassword() {
        if (parentService.checkPassword(passwortOld, person2)) {
            successMsg = "Die Änderungen an dem/der Erziehungsberechtigten " + person2 + " wurden gespeichert.";
            this.doSavePerson2();
        } else {
            successMsg = "altes Passwort inkorrekt";
            success = false;
        }
    }

}
