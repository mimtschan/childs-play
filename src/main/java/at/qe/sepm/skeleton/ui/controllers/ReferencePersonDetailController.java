package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Child;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.ReferencePerson;
import at.qe.sepm.skeleton.services.ReferencePersonService;
import org.primefaces.model.DualListModel;

/**
 * Controller for the user detail view.
 *
 * This class is part of the skeleton project provided for students of the
 * course "Softwaredevelopment and Project Management" offered by the University
 * of Innsbruck.
 */
@Component
@Scope("view")
public class ReferencePersonDetailController extends PersonDetailController<ReferencePerson> {

    @Autowired
    private ReferencePersonService referencePersonService;

    private DualListModel<Child> children;

    @Override
    protected ReferencePersonService getGenericService() {
        return referencePersonService;
    }

    @Override
    protected ReferencePerson createGenericPerson() {
        return referencePersonService.createReferencePerson();
    }

    public DualListModel<Child> getChildren() {
        if (children == null) {
            //children = new DualListModel<>(childService.getAllChilds(), this.person.getChilds());
            //children.getSource().removeAll(this.person.getChilds());
        }
        return children;
    }

    /**
     * @param children the children to set
     */
    public void setChildren(DualListModel<Child> children) {
        this.children = children;
        //this.person.setChilds(children.getTarget());
    }

    @Override
    public boolean isValid() {
        return super.isValid()
                && doCheckAttribute(person.getEmail(), 5)
                && person.getPhonePrivate() != null
                && doCheckAttribute(person.getStreet(), 3)
                && doCheckAttribute(person.getCity(), 1)
                && doCheckAttribute(person.getZipcode(), 3);
    }

    @Override
    public void doSavePerson() {
        if (isValid() && person.isNew()) {
            successMsg = "Der/die neue Bezugsperson " + person.getFirstName() + " " + person.getLastName() + " wurde angelegt.";
        } else if (isValid()) {
            successMsg = "Die Änderungen an dem/der Bezugsperson " + person.getFirstName() + " " + person.getLastName() + " wurden gespeichert.";
        }
        super.doSavePerson();
    }

    public void doConfirm() {
        referencePersonService.confirmReferencePerson(person);
    }

}
