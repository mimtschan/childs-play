package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Person;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("view")
public class SessionViewController {

    @Autowired
    private SessionInfoBean sessionInfoBean;

    @Autowired
    private UserService userService;

    public Person getPerson() {
        return userService.loadUser(sessionInfoBean.getCurrentUserName()).getPerson();
    }

}
