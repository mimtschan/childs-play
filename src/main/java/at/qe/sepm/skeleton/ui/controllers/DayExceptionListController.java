package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.DayException;
import at.qe.sepm.skeleton.services.DayExceptionService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Controller for the user list view.
 *
 * This class is part of the skeleton project provided for students of the
 * course "Softwaredevelopment and Project Management" offered by the University
 * of Innsbruck.
 */
@Component
@Scope("view")
public class DayExceptionListController {

    @Autowired
    private DayExceptionService dayExceptionService;

    private List<DayException> dayExceptionList;

    /**
     * @return the taskList
     */
    public List<DayException> getDayExceptionList() {
        this.doReloadDayExceptionList();
        return dayExceptionList;
    }

    public void doReloadDayExceptionList() {
        dayExceptionList = dayExceptionService.getAllExceptions();
    }

}
