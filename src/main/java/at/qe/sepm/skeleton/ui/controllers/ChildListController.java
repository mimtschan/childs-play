package at.qe.sepm.skeleton.ui.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.services.ChildService;
import at.qe.sepm.skeleton.services.PersonBasedService;
import java.util.Map;
import java.util.TreeMap;

@Component
@Scope("view")
public class ChildListController extends PersonListController<Child> {

    @Autowired
    private ChildService childService;

//    private List<Child> children;
//    @PostConstruct
//    private void init() {
//        children = childService.getAllActiveChilds();
//    }
//    public Collection<Child> getChildren() {
//        return children;
//    }
    @Override
    public Map<String, Integer> getFilters() {
        Map<String, Integer> filters = new TreeMap<>();
        filters.put("Nur aktive Kinder", FILT_ACT);
        filters.put("Alle Kinder", FILT_ALL);
        return filters;
    }

    @Override
    public PersonBasedService<Child> getGenericService() {
        return childService;
    }

    @Override
    public void doReloadPersonList() {
        if (filter == FILT_ACT) {
            personList = childService.getAllActiveChildren();
        } else {
            personList = childService.getAllChildren();
        }
    }

}
