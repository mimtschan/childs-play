package at.qe.sepm.skeleton.ui.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Person;
import at.qe.sepm.skeleton.services.PersonBasedService;
import at.qe.sepm.skeleton.services.UserService;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * Controller for the user detail view.
 *
 * This class is part of the skeleton project provided for students of the
 * course "Softwaredevelopment and Project Management" offered by the University
 * of Innsbruck.
 *
 * @param <T> dtype of person
 */
@Component
@Scope("view")
public abstract class PersonDetailController<T extends Person> {

    @Autowired
    private UserService userService;

    /**
     * Attribute to cache the currently displayed person
     */
    protected T person;
    //person 2 attribute needed for PW changing screen. otherwise false
    //validations would be rendered
    protected T person2;
    protected String successMsg;
    protected String successHeader;
    protected boolean success;
    protected boolean isNew;

    protected abstract PersonBasedService<T> getGenericService();

    protected abstract T createGenericPerson();

    public T getPerson() {
        return person;
    }

    public T getPerson2() {
        return person2;
    }

    public void setPerson(T person) {
        successMsg = "Bitte prüfen Sie, ob alle Pflichtfelder ausgefüllt sind.";
        successHeader = "Fehler beim Bearbeiten";
        success = false;
        isNew = false;

        this.person = person;
        doReloadPerson();
    }

    public void setPerson2(T person) {
        successMsg = "Bitte prüfen Sie, ob alle Pflichtfelder ausgefüllt sind.";
        successHeader = "Fehler beim Bearbeiten";
        success = false;
        isNew = false;

        this.person2 = person;
    }

    public void setNewPerson() {
        successMsg = "Fehler bei der Registrierung. Bitte prüfen Sie, ob alle Pflichtfelder ausgefüllt sind.";
        successHeader = "Fehler bei der Registrierung";
        success = false;
        isNew = true;
        this.person = createGenericPerson();
    }

    public String getSuccessMsg() {
        return successMsg;
    }

    public String getSuccessHeader() {
        return successHeader;
    }

    public boolean getSuccess() {
        return success;
    }

    public boolean getIsNew() {
        return isNew;
    }

    public void doReloadPerson() {
        person = getGenericService().loadPersonById(person.getPersonId());
    }

    public void doSavePerson() {
        if (isValid() && person.isNew()) {
            successHeader = "Registrierung erfolgreich";
            person = getGenericService().savePerson(person);
            success = true;
        } else if (isValid()) {
            successHeader = "Änderungen gespeichert";
            person = getGenericService().savePerson(person);
            success = true;
        }
    }

    public void doSavePerson2() {
        if (person2.getUser().getPassword() != null) {
            successHeader = "PW gespeichert";
            successMsg = "Passwort erfolgreich geändert";
            person2 = getGenericService().savePerson(person2);
            success = true;
            person2 = null;
        }
    }

    public void doDeletePerson() {
        this.getGenericService().deletePerson(person);
        person = null;
        success = false;
    }

    public boolean isValid() {
        return doCheckAttribute(person.getFirstName(), 1)
                && doCheckAttribute(person.getLastName(), 1)
                && person.getGender() != null;
    }

    public boolean doCheckAttribute(String attribute, int min) {
        return attribute != null;
        //&& attribute.length() > min;
    }

    public void checkDuplicateUsername() {
        if (!person.isNew()) {
            return;
        } else if (userService.loadUser(person.getUser().getUsername()) != null) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage("msgUsername", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Username existiert schon", null));
        }
    }

    public boolean isUsernameValid(String username) {
        if (!person.isNew()) {
            return true;
        } else if (username.length() < 4) {
            return false;
        } else {
            return !(userService.getAllUsers().contains(userService.loadUser(username)));
        }
    }

    public void doCancel() {
        person = null;
        person2 = null;
    }

}
