package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.ChildReferencePersonRelation;
import at.qe.sepm.skeleton.services.ChildRefPersonRelationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("view")
public class ChildRefPersonRelationshipDetailController {

    @Autowired
    private ChildRefPersonRelationService childRefPersonRelationService;

    /**
     * Attribute to cache the currently displayed person
     */
    protected ChildReferencePersonRelation childRefPersonRelation;
    protected String successMsg;
    protected String successHeader;
    protected boolean success;

    public int sortAlphabetically(String a, String b) {
        return String.CASE_INSENSITIVE_ORDER.compare(a, b);
    }

    protected ChildReferencePersonRelation createChildRefPersonRelation() {
        return childRefPersonRelationService.createRelation();
    }

    public ChildReferencePersonRelation getChildRefPersonRelation() {
        return childRefPersonRelation;
    }

    public void setChildRefPersonRelation(ChildReferencePersonRelation childReferencePersonRelation) {
        successMsg = "Bitte prüfen Sie, ob alle Pflichtfelder ausgefüllt sind.";
        successHeader = "Fehler beim Bearbeiten";
        success = false;

        this.childRefPersonRelation = childReferencePersonRelation;
        doReloadChildRefPersonRelation();
    }

    public void setNewChildRefPersonRelation() {
        successMsg = "Fehler beim Anlegen der Bezugsperson. Bitte prüfen Sie, ob alle Pflichtfelder ausgefüllt sind.";
        successHeader = "Fehler beim Anlegen";
        success = false;
        this.childRefPersonRelation = createChildRefPersonRelation();
    }

    public String getSuccessMsg() {
        return successMsg;
    }

    public String getSuccessHeader() {
        return successHeader;
    }

    public boolean getSuccess() {
        return success;
    }

    public void doReloadChildRefPersonRelation() {
        if (childRefPersonRelation != null && !childRefPersonRelation.isNew()) {
            childRefPersonRelation = childRefPersonRelationService.getSingle(childRefPersonRelation.getId());
        }
    }

    public void doSaveRelation() {
        if (doCheckIfValid() && childRefPersonRelation.isNew()) {
            successMsg = "Die Bezugsperson wurde erfolgreich angelegt.";
            successHeader = "Bezugsperson angelegt";
            childRefPersonRelation = childRefPersonRelationService.saveRelation(childRefPersonRelation);
            success = true;
        } else if (doCheckIfValid()) {
            successMsg = "Die Änderungen an der Bezugsperson wurden gespeichert";
            successHeader = "Änderungen gespeichert";
            childRefPersonRelation = childRefPersonRelationService.saveRelation(childRefPersonRelation);
            success = true;
        }
    }

    public void doDeleteRelation() {
        this.childRefPersonRelationService.deleteRelation(childRefPersonRelation);
        childRefPersonRelation = null;
        success = false;
    }

    public boolean doCheckIfValid() {
        return true;
    }

    public boolean doCheckAttribute(String attribute, int min) {
        return attribute != null;
        //&& attribute.length() > min;
    }

    public void doCancel() {
        this.childRefPersonRelation = null;
    }

}
