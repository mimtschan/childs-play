package at.qe.sepm.skeleton.ui.controllers;

import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Parent;
import at.qe.sepm.skeleton.services.ParentService;

/**
 * Controller for the user list view.
 *
 * This class is part of the skeleton project provided for students of the
 * course "Softwaredevelopment and Project Management" offered by the University
 * of Innsbruck.
 */
@Component
@Scope("view")
public class ParentListController extends PersonListController<Parent> {

    @Autowired
    private ParentService parentService;

    @Override
    public Map<String, Integer> getFilters() {
        Map<String, Integer> filters = new TreeMap<>();
        filters.put("Nur aktive Erziehungsberechtigte", FILT_ACT);
        filters.put("Nur inaktive Erziehungsberechtigte", FILT_INACT);
        filters.put("Alle Erziehungsberechtigten", FILT_ALL);
        return filters;
    }

    @Override
    public void doReloadPersonList() {
        if (filter == FILT_ACT) {
            personList = parentService.getAllActiveParents();
        } else if (filter == FILT_INACT) {
            personList = parentService.getAllInactiveParents();
        } else {
            personList = parentService.getAllParents();
        }
    }

    @Override
    protected ParentService getGenericService() {
        return parentService;
    }

}
