package at.qe.sepm.skeleton.ui.controllers;

import java.util.Collection;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Person;
import at.qe.sepm.skeleton.services.PersonBasedService;
import java.util.ArrayList;
import java.util.Map;
import org.primefaces.model.DualListModel;

/**
 * Controller for the user list view.
 *
 * This class is part of the skeleton project provided for students of the
 * course "Softwaredevelopment and Project Management" offered by the University
 * of Innsbruck.
 */
@Component
@Scope("view")
public abstract class PersonListController<T extends Person> {

    protected List<T> personList;
    protected List<T> filteredPersons;
    protected DualListModel<T> personPickList;

    protected final int FILT_ALL = 0;
    protected final int FILT_ACT = 1;
    protected final int FILT_INACT = 2;

    protected int filter;

    protected abstract PersonBasedService<T> getGenericService();

    protected abstract void doReloadPersonList();

    public Map<String, Integer> getFilters() {
        return null;
    }

    public void setFilter(int filter) {
        this.filter = filter;
        doReloadPersonList();
    }

    public int getFilter() {
        return filter;
    }

    public List<T> getFilteredPersons() {
        return filteredPersons;
    }

    public void setFilteredPersons(List<T> filteredPersons) {
        this.filteredPersons = filteredPersons;
    }

    public Collection<T> getPersonList() {
        if (personList == null) {
            doReloadPersonList();
        }
        return personList;
    }

    public int sortByName(Person a, Person b) {
        int order = String.CASE_INSENSITIVE_ORDER.compare(a.getLastName(), b.getLastName());
        if (order == 0) {
            return String.CASE_INSENSITIVE_ORDER.compare(a.getFirstName(), b.getFirstName());
        } else {
            return order;
        }
    }

    public int sortAlphabetically(String a, String b) {
        return String.CASE_INSENSITIVE_ORDER.compare(a, b);
    }

    public DualListModel<T> getPersonPickList(List<T> target) {
        personPickList.setSource(new ArrayList<T>());

        for (T s : getPersonList()) {
            if (!target.contains(s)) {
                personPickList.getSource().add(s);
            }
        }

        personPickList = new DualListModel<T>(personPickList.getSource(), target);
        return personPickList;
    }

    public DualListModel<T> getPersonPickList(List<T> target, T current) {
        personPickList = getPersonPickList(target);
        personPickList.getSource().remove(current);
        return personPickList;
    }

    public void setPersonPickList(DualListModel<T> personPickList) {
        this.personPickList = personPickList;
    }

    public List<T> getPersonCreatedBy(T person) {
        return getGenericService().getCreatedBy(person);
    }

}
