package at.qe.sepm.skeleton.ui.controllers;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Configuration;
import at.qe.sepm.skeleton.model.ConfigurationTimes;
import at.qe.sepm.skeleton.model.Weekday;
import at.qe.sepm.skeleton.services.ConfigurationService;
import java.util.TreeMap;

/**
 * Controller for the user detail view.
 *
 * This class is part of the skeleton project provided for students of the
 * course "Softwaredevelopment and Project Management" offered by the University
 * of Innsbruck.
 */
@Component
@Scope("view")
public class ConfigurationController {

    @Autowired
    private ConfigurationService configService;

    private Configuration config;
    private ConfigurationTimes monday;
    private ConfigurationTimes tuesday;
    private ConfigurationTimes wednesday;
    private ConfigurationTimes thursday;
    private ConfigurationTimes friday;
    private ConfigurationTimes saturday;
    private ConfigurationTimes sunday;

    private Map<String, String> themes;

    @PostConstruct
    public void setup() {
        this.config = configService.getConfig();
        Map<Weekday, ConfigurationTimes> times = configService.getAllTimeConfigs();
        for (Entry<Weekday, ConfigurationTimes> time : times.entrySet()) {
            try {
                Field dynamicField = this.getClass().getDeclaredField(time.getKey().name().toLowerCase());
                dynamicField.set(this, time.getValue());
            } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @return the config
     */
    public Configuration getConfig() {
        return config;
    }

    /**
     * @return the monday
     */
    public ConfigurationTimes getMonday() {
        return monday;
    }

    /**
     * @return the tuesday
     */
    public ConfigurationTimes getTuesday() {
        return tuesday;
    }

    /**
     * @return the wednesday
     */
    public ConfigurationTimes getWednesday() {
        return wednesday;
    }

    /**
     * @return the thursday
     */
    public ConfigurationTimes getThursday() {
        return thursday;
    }

    /**
     * @return the friday
     */
    public ConfigurationTimes getFriday() {
        return friday;
    }

    /**
     * @return the saturday
     */
    public ConfigurationTimes getSaturday() {
        return saturday;
    }

    /**
     * @return the sunday
     */
    public ConfigurationTimes getSunday() {
        return sunday;
    }

    public void doSaveConfiguration() {
        configService.saveConfig(config);
    }

    public Map<String, String> getThemes() {
        themes = new TreeMap<>();
        themes.put("Afterdark", "afterdark");
        themes.put("Afternoon", "afternoon");
        themes.put("Afterwork", "afterwork");
        themes.put("Aristo", "aristo");
        themes.put("Black-Tie", "black-tie");
        themes.put("Blitzer", "blitzer");
        themes.put("Bluesky", "bluesky");
        themes.put("Bootstrap", "bootstrap");
        themes.put("Casablanca", "casablanca");
        themes.put("Cupertino", "cupertino");
        themes.put("Cruze", "cruze");
        themes.put("Dark-Hive", "dark-hive");
        themes.put("Delta", "delta");
        themes.put("Dot-Luv", "dot-luv");
        themes.put("Eggplant", "eggplant");
        themes.put("Excite-Bike", "excite-bike");
        themes.put("Flick", "flick");
        themes.put("Glass-X", "glass-x");
        themes.put("Home", "home");
        themes.put("Hot-Sneaks", "hot-sneaks");
        themes.put("Humanity", "humanity");
        themes.put("Le-Frog", "le-frog");
        themes.put("Midnight", "midnight");
        themes.put("Mint-Choc", "mint-choc");
        themes.put("Overcast", "overcast");
        themes.put("Pepper-Grinder", "pepper-grinder");
        themes.put("Redmond", "redmond");
        themes.put("Rocket", "rocket");
        themes.put("Sam", "sam");
        themes.put("Smoothness", "smoothness");
        themes.put("South-Street", "south-street");
        themes.put("Start", "start");
        themes.put("Sunny", "sunny");
        themes.put("Swanky-Purse", "swanky-purse");
        themes.put("Trontastic", "trontastic");
        themes.put("UI-Darkness", "ui-darkness");
        themes.put("UI-Lightness", "ui-lightness");
        themes.put("Vader", "vader");
        return themes;
    }

}
