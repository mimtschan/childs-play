package at.qe.sepm.skeleton.comparators;

import java.util.Comparator;

import at.qe.sepm.skeleton.model.Person;

/**
 * Comperator which orders 2 Person instances, by comparing their lastname. If
 * lastname is equal, their firstname will be compared
 *
 * @author tade
 */
public class PersonNameComparator implements Comparator<Person> {

    @Override
    public int compare(Person arg0, Person arg1) {
        int ret = arg0.getLastName().compareTo(arg1.getLastName());
        if (ret == 0) {
            ret = arg0.getFirstName().compareTo(arg1.getFirstName());
        }
        return ret;
    }

}
