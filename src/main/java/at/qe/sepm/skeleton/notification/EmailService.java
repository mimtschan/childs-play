package at.qe.sepm.skeleton.notification;

import at.qe.sepm.skeleton.model.DayException;
import at.qe.sepm.skeleton.model.Person;
import at.qe.sepm.skeleton.model.Staff;
import at.qe.sepm.skeleton.model.Task;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.temporary.Confirmation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableAsync
@EnableScheduling
public class EmailService {

    public class EmailRunner implements Runnable {

        private final JavaMailSender emailSender;

        private final SimpleMailMessage message;

        public EmailRunner(JavaMailSender emailSender, SimpleMailMessage message) {
            this.emailSender = emailSender;
            this.message = message;
        }

        @Override
        public void run() {
            emailSender.send(this.message);
        }

    }

    @Autowired
    private TaskExecutor taskExecutor;
    @Autowired
    private JavaMailSender emailSender;
    @Autowired
    private EmailTemplates emailTemplates;

    @Async
    public void sendSimpleMessage(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        taskExecutor.execute(new EmailRunner(emailSender, message));
    }

    @Async
    public void sendSimpleMessage(Task task) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(task.getAssigned().getEmail());
        message.setSubject(task.getTitle());
        message.setText(emailTemplates.getTaskTemplate(task));
        taskExecutor.execute(new EmailRunner(emailSender, message));
    }

    @Async
    public void sendSimpleMessage(Confirmation confirmation, Staff decliner) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(confirmation.getCreatorsEmail());
        message.setSubject("Abgelehnt: " + confirmation.getTypeText());
        message.setText(emailTemplates.getConfirmationDeclineTemplate(confirmation, decliner));
        taskExecutor.execute(new EmailRunner(emailSender, message));
    }

    @Async
    public void sendSimpleMessage(User user, Person personOfUser, String password) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(personOfUser.getEmail());
        message.setSubject("Ihre Zugangsdaten für Child's Play");
        message.setText(emailTemplates.getNewUserTemplate(user, password));
        taskExecutor.execute(new EmailRunner(emailSender, message));
    }

    @Async
    public void sendSimpleMessage(DayException dayException, Person decliner) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(dayException.getCreatedBy().getEmail());
        message.setSubject("Abgelehnt: " + dayException.getDescription());
        message.setText(emailTemplates.getDayExceptionDeclineTemplate(dayException, decliner));
        taskExecutor.execute(new EmailRunner(emailSender, message));
    }

}
