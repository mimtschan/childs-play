package at.qe.sepm.skeleton.notification;

import at.qe.sepm.skeleton.model.DayException;
import at.qe.sepm.skeleton.model.Person;
import at.qe.sepm.skeleton.model.Staff;
import at.qe.sepm.skeleton.model.Task;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.temporary.Confirmation;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EmailTemplates {

    String getTaskTemplate(Task task) {
        return "Hallo " + task.getAssigned() + "\n"
                + "\n"
                + "Dir wurde die Arbeit " + task.getDescription() + " zugeteilt";
    }

    String getConfirmationDeclineTemplate(Confirmation confirmation, Staff decliner) {
        return "Hallo\n"
                + "\n"
                + "Ihre Anfrage für " + confirmation.getTypeText() + " " + confirmation.getText()
                + " wurde von " + decliner.toString() + " abgelehnt";
    }

    String getNewUserTemplate(User user, String password) {
        return "Hallo\n"
                + "\n"
                + "Ihr Benutzername\t" + user.getUsername()
                + "\nIhr Password\t" + password;
    }

    String getDayExceptionDeclineTemplate(DayException dayException, Person decliner) {
        return "Hallo\n"
                + "\n"
                + "Ihre Anfrage für " + dayException.getDescription() + "\n"
                + dayException.getChild() + " "
                + dayException.getDate() + "\n"
                + " wurde von " + decliner + " abgelehnt";
    }

}
