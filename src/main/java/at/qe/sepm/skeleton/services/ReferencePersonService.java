package at.qe.sepm.skeleton.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.ReferencePerson;
import at.qe.sepm.skeleton.model.UserRole;
import at.qe.sepm.skeleton.notification.EmailService;
import at.qe.sepm.skeleton.repositories.ReferencePersonRepository;

@Component
@Scope("application")
public class ReferencePersonService extends PersonBasedService<ReferencePerson> {

    @Autowired
    ReferencePersonRepository refPersRepo;
    @Autowired
    EmailService emailService;

    @PreAuthorize("hasAuthority('PARENT') or hasAuthority('STAFF')")
    public List<ReferencePerson> getAllReferencePersons() {
        return refPersRepo.findAll();
    }

    @PreAuthorize("hasAuthority('PARENT') or hasAuthority('STAFF')")
    public ReferencePerson createReferencePerson() {
        return createPerson();
    }

    @PreAuthorize("hasAuthority('PARENT') or hasAuthority('STAFF')")
    public List<ReferencePerson> getAllUnconfirmedReferencePerson() {
        return refPersRepo.findAllByEnabledIsFalse();
    }

    @PreAuthorize("hasAuthority('PARENT') or hasAuthority('STAFF')")
    public List<ReferencePerson> getAllConfirmedReferencePerson() {
        return refPersRepo.findAllByEnabledIsTrue();
    }

    @Override
    protected ReferencePerson instantiate() {
        return new ReferencePerson();
    }

    public ReferencePerson confirmReferencePerson(ReferencePerson person) {
        person.setEnabled(true);
        return super.savePerson(person);
    }

    @PreAuthorize("hasAuthority('STAFF') or hasAuthority('INACTPARENT')")
    @Override
    public ReferencePerson savePerson(ReferencePerson referencePerson) {
        if (referencePerson.isNew()) {
            //Set Enable True if acutal User is a Staff, false otherwise
            referencePerson.setEnabled(userService.getAuthenticatedUser().getRoles().contains(UserRole.STAFF));
        }
        return super.savePerson(referencePerson);
    }

}
