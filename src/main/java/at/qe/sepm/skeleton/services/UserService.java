package at.qe.sepm.skeleton.services;

import java.util.Collection;
import java.util.List;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.UserRole;
import at.qe.sepm.skeleton.repositories.UserRepository;

/**
 * Service for accessing and manipulating user data.
 *
 * This class is part of the skeleton project provided for students of the
 * course "Softwaredevelopment and Project Management" offered by the University
 * of Innsbruck.
 */
@Component
@Scope("application")
public class UserService {

    @Autowired
    private UserRepository userRepo;
    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * Returns a collection of all users.
     *
     * @return
     */
    @PreAuthorize("hasAuthority('STAFF')")
    public Collection<User> getAllUsers() {
        return userRepo.findAll();
    }

    /**
     * Loads a single user identified by its username.
     *
     * @param username the username to search for
     * @return the user with the given username
     */
    @PreAuthorize("hasAuthority('STAFF') or principal.username eq #username")
    public User loadUser(String username) {
        return userRepo.findFirstByUsername(username);
    }

    /**
     * @param user the user to save
     * @return the updated user
     */
    @PreAuthorize("hasAuthority('STAFF') or hasAuthority('INACTPARENT')")
    public User saveUser(User user) {
        if (user.isNew()) {
        }
        return userRepo.save(user);
    }

    /**
     * Deletes the user.
     *
     * @param user the user to delete
     */
    @PreAuthorize("hasAuthority('STAFF')")
    public void deleteUser(User user) {
        userRepo.delete(user);
        // :TODO: write some audit log stating who and when this user was
        // permanently deleted.
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public List<User> getInactiveParentUsers() {
        return userRepo.findByRoleAndNotRole(UserRole.INACTPARENT, UserRole.PARENT);
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public List<User> getActiveParentUsers() {
        return userRepo.findByRole(UserRole.PARENT);
    }

    public User getAuthenticatedUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return userRepo.findFirstByUsername(auth.getName());
    }

    public User createUser() {
        User newUser = new User();
        newUser.setEnabled(true);
        newUser.setNew(true);
        newUser.setRoles(new TreeSet<>());
        newUser.setPasswordEncoder(passwordEncoder);
        return newUser;
    }

}
