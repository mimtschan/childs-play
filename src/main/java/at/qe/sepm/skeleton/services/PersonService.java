package at.qe.sepm.skeleton.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Person;
import org.springframework.security.access.prepost.PreAuthorize;

/**
 * This Service delivers information information over all personbased entities
 * The other services only give information for the specified person type.
 *
 * @author tade
 */
@Component
@Scope("application")
public class PersonService {

    @Autowired
    ParentService parentService;
    @Autowired
    StaffService staffService;
    @Autowired
    ReferencePersonService refPersService;

    /**
     * Gives a list of all persons in the system
     *
     * @return Person List
     */
    @PreAuthorize("hasAuthority('STAFF')")
    public List<Person> getAllPersons() {
        List<Person> personList = new ArrayList<>();
        personList.addAll(parentService.getAllParents());
        personList.addAll(refPersService.getAllReferencePersons());
        personList.addAll(staffService.getAllStaff());
        return personList;
    }

    /**
     * Gives a list of all relevant contact persons The contact persons are all
     * active parents and all confirmed reference persons
     *
     * @return Person List
     */
    @PreAuthorize("hasAuthority('STAFF')")
    public List<Person> getAllContactPersons() {
        List<Person> personList = new ArrayList<>();
        personList.addAll(parentService.getAllActiveParents());
        personList.addAll(refPersService.getAllConfirmedReferencePerson());
        return personList;
    }

}
