package at.qe.sepm.skeleton.services;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.Weekday;
import at.qe.sepm.skeleton.repositories.ChildRepository;
import java.util.Date;

@Component
@Scope("application")
public class ChildService extends PersonBasedService<Child> {

    @Autowired
    ChildRepository childRepo;

    @PreAuthorize("hasAuthority('STAFF')")
    public List<Child> getAllChildren() {
        return childRepo.findAll();
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public List<Child> getAllActiveChildren() {
        return childRepo.findAllByEnabledIsTrue();
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public List<Child> getAllActiveChildrenOnWeekday(Weekday weekday) {
        return childRepo.findByWeekdayAndEnabled(weekday);
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public Child createChild() {
        Child child = createPerson();
        child.setRegistrationDate(new Date());
        return child;
    }

    @Override
    protected Child instantiate() {
        Child newChild = new Child();
        //initialize empty connection tables
        newChild.setSiblings(new ArrayList<>());
        newChild.setSubDates(new TreeSet<>());
        newChild.setParents(new ArrayList<>());
        return newChild;
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public int getActualCapacityForWeekday(Weekday weekday) {
        return getAllActiveChildrenOnWeekday(weekday).size();
    }

}
