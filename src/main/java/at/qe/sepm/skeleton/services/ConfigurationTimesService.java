package at.qe.sepm.skeleton.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.ConfigurationTimes;
import at.qe.sepm.skeleton.repositories.ConfigurationTimesRepository;

@Component
@Scope("application")
public class ConfigurationTimesService {

    @Autowired
    ConfigurationTimesRepository configTimesRepo;

    public List<ConfigurationTimes> getTimeConfigs() {
        return configTimesRepo.findAll();
    }

    public ConfigurationTimes save(ConfigurationTimes cap) {
        return configTimesRepo.save(cap);
    }

}
