package at.qe.sepm.skeleton.services;

import java.util.Set;

import at.qe.sepm.skeleton.model.Person;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.UserRole;
import at.qe.sepm.skeleton.notification.EmailService;
import java.math.BigInteger;
import java.security.SecureRandom;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * This Service contains all functionality for persons which are connected to a
 * user. If a person is created, deleted or changed, also actions on the user
 * entity should be done
 *
 * @author tade
 * @param <T> specific Person type with user connection
 */
public abstract class UserBasedService<T extends Person> extends PersonBasedService<T> {

    @Autowired
    private EmailService emailService;

    private final SecureRandom random = new SecureRandom();

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void deletePerson(T person) {
        if (!person.isAdmin()) {	//deletion of Admin is not allowed!
            super.deletePerson(person);
            //delete also connected user
            userService.deleteUser(person.getUser());
        }
    }

    @Override
    public T savePerson(T person) {
        User personsUser = person.getUser();
        if (personsUser.isNew()) {
            //If new, generate password, and send email 
            //with password in textform before encryption
            String newPw = generatePassword();
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, "New Password for " + personsUser.getUsername() + ": " + newPw);
            emailService.sendSimpleMessage(personsUser, person, newPw);
            personsUser.setPassword(newPw);
        }
        //save also User before person
        userService.saveUser(personsUser);
        return super.savePerson(person);
    }

    @Override
    public T createPerson() {
        //If person is created, also create a user and connect them
        T newPerson = super.createPerson();
        createUser(newPerson);
        return newPerson;
    }

    /**
     * Creates fully initialized user instance
     *
     * @param person where the user is connected to
     */
    protected void createUser(T person) {
        User newUser = userService.createUser();
        initBasicRoles(newUser.getRoles());
        person.setUser(newUser);
    }

    /**
     * Set up the initial rols of the user
     *
     * @param roles Roles which are changed
     */
    protected abstract void initBasicRoles(Set<UserRole> roles);

    /**
     * Basic check, whether the user of a given person has a given role
     *
     * @param person Person wich user connection
     * @param role Role for check
     * @return True if Persons user has the role
     */
    public boolean hasRole(T person, UserRole role) {
        return person.getUser().getRoles().contains(role);
    }

    /**
     * Generates a new random password
     *
     * @return unencrypted password string
     */
    private String generatePassword() {
        return new BigInteger(130, random).toString(32);
    }

    /**
     * Check wheater a given password fits the password of a persons user
     *
     * @param password unencryped password
     * @param person person with user connection
     * @return True if the password fits the hashed value
     */
    public boolean checkPassword(String password, T person) {
        if (password == null) {
            return false;
        }
        return passwordEncoder.matches(password, this.loadPersonById(person.getPersonId()).getUser().getPassword());
    }

}
