package at.qe.sepm.skeleton.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Parent;
import at.qe.sepm.skeleton.model.Staff;
import at.qe.sepm.skeleton.model.Task;
import at.qe.sepm.skeleton.notification.EmailService;
import at.qe.sepm.skeleton.repositories.TaskRepository;

@Component
@Scope("application")
public class TaskService {

    @Autowired
    TaskRepository taskRepo;
    @Autowired
    UserService userService;
    @Autowired
    EmailService emailService;

    @PreAuthorize("hasAuthority('STAFF')")
    public List<Task> getAllTasks() {
        return taskRepo.findAll();
    }

    @PreAuthorize("hasAuthority('PARENT')")
    public List<Task> getTasksForParent(Parent parent) {
        return taskRepo.findAllByAssignedAndDoneIsNull(parent);
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public List<Task> getOpenTasks() {
        return taskRepo.findAllByDoneIsNull();
    }

    public Task loadSingle(long id) {
        return taskRepo.findOne(id);
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public Task createTask() {
        Task newTask = new Task();
        Date today = new Date();
        //preset Dates
        newTask.setFromDate(today);
        newTask.setToDate(today);
        return newTask;
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public void delete(Task task) {
        taskRepo.delete(task);
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public Task save(Task task) {
        if (task.isNew()) {
            task.setCreateDate(new Date());
            task.setAssignedBy((Staff) userService.getAuthenticatedUser().getPerson());
            emailService.sendSimpleMessage(task);
        } else {
            task.setUpdateDate(new Date());
        }
        return taskRepo.save(task);
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public void completeTask(Task task) {
        task.setDone(new Date());
    }

}
