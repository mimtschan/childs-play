package at.qe.sepm.skeleton.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.comparators.PersonNameComparator;
import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.ConfigurationTimes;
import at.qe.sepm.skeleton.model.DayException;
import at.qe.sepm.skeleton.model.Weekday;
import at.qe.sepm.skeleton.model.temporary.ConfigDayInformation;
import at.qe.sepm.skeleton.model.temporary.DayExceptionDayInformation;
import at.qe.sepm.skeleton.model.temporary.DayInformation;

/**
 * This class retrieves data for the daily or monthly info for the staff.
 *
 * @author tade
 */
@Component
@Scope("application")
public class DayPlannerService {

    @Autowired
    private ConfigurationService configService;
    @Autowired
    private ChildService childService;
    @Autowired
    private DayExceptionService dayExceptionService;

    /**
     * Retrieves special data for all active Childs for exactly 1 day
     *
     * @param date of which info shall be retrieved
     * @return Day Information for each child
     */
    public Map<Child, DayInformation> getDayInformation(Date date) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        Map<Child, DayInformation> data = new TreeMap<>(new PersonNameComparator());
        List<Child> children = childService.getAllActiveChildren();

        children.forEach(child -> data.put(child, this.getChildDayInformation(child, cal)));
        return data;
    }

    /**
     * Retrives Dayinformation for all active Childs for a whole month in
     *
     * @param someDayInMonth is used to find which month is relevant
     * @return Map of Children an Info for each day
     */
    public Map<Child, List<DayInformation>> getMonthDayInformation(Calendar someDayInMonth) {
        Calendar firstMonthDay = (Calendar) someDayInMonth.clone();
        firstMonthDay.set(Calendar.DAY_OF_MONTH, 1);
        Calendar lastMonthDay = (Calendar) firstMonthDay.clone();
        lastMonthDay.add(Calendar.MONTH, 1);
        return getRangeDayInformation(firstMonthDay, lastMonthDay);
    }

    /**
     * Retrives Dayinformation for all active Childs between date - 7 days and
     * date + 21 days.
     *
     * @param someDayInMonth some date, from which the range calculation starts
     * @return Map of Children an Info for each day
     */
    public Map<Child, List<DayInformation>> getVariableMonthDayInformation(Calendar someDayInMonth) {
        Calendar firstMonthDay = (Calendar) someDayInMonth.clone();
        firstMonthDay.add(Calendar.DAY_OF_MONTH, -7);
        Calendar lastMonthDay = (Calendar) someDayInMonth.clone();
        lastMonthDay.add(Calendar.DAY_OF_MONTH, 21);
        return getRangeDayInformation(firstMonthDay, lastMonthDay);
    }

    /**
     * Retrieves Information for all active Childs within the given range
     *
     * @param from Start of the range
     * @param to end of the range
     * @return
     */
    public Map<Child, List<DayInformation>> getRangeDayInformation(Calendar from, Calendar to) {

        Map<Child, List<DayInformation>> data = new TreeMap<>(new PersonNameComparator());
        List<Calendar> allDays = createDateList(from, to);

        for (Child child : childService.getAllActiveChildren()) {
            List<DayInformation> childData = new ArrayList<>(allDays.size());
            for (Calendar day : allDays) {
                DayInformation singleData = getChildDayInformation(child, day);
                if (singleData != null) {
                    childData.add(singleData);
                }
            }
            data.put(child, childData);
        }

        return data;
    }

    /**
     * Creates a list of dates for the given range
     *
     * @param start start of the range
     * @param end end of the range
     * @return List of Calendar-Objects, with entry for each day in between
     */
    public List<Calendar> createDateList(Calendar start, Calendar end) {
        int diff = (int) ((end.getTimeInMillis() - start.getTimeInMillis()) / (1000 * 60 * 60 * 24));
        List<Calendar> data = new ArrayList<>(diff);
        for (int i = 0; i < diff; i++) {
            Calendar instance = (Calendar) start.clone();
            instance.add(Calendar.DATE, i);
            instance.set(Calendar.HOUR_OF_DAY, 12);
            data.add(instance);
        }
        return data;
    }

    private DayInformation getChildDayInformation(Child child, Calendar day) {
        // is generally open on this day?
        if (!configService.isOpenOn(day.getTime())) {
            return configService.getClosedReason(day.getTime());
        }

        Weekday weekday = Weekday.fromCalendar(day);
        ConfigurationTimes weekdayConfig = configService.getForWeekday(weekday);
        // has child some exception on this day?
        DayException info = dayExceptionService.getSingleForChildAndDay(child, day.getTime());
        if (info == null) {
            // if no exception...
            // ..is child here on this weekday?
            if (child.getSubDates().contains(weekday)) {
                // if yes, return defaul configuration
                return new ConfigDayInformation((Calendar) day.clone(), weekdayConfig);
            }
        } else {
            return new DayExceptionDayInformation(day, weekdayConfig, info);
        }
        // otherwise no information
        return null;
    }

}
