package at.qe.sepm.skeleton.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Holiday;
import at.qe.sepm.skeleton.repositories.HolidayRepository;

@Component
@Scope("application")
public class HolidayService {

    @Autowired
    HolidayRepository holidayRepo;

    public List<Holiday> getAllHolidays() {
        return holidayRepo.findAll();
    }

    public Holiday loadSingle(Date date) {
        return holidayRepo.findOne(date);
    }

    public Holiday save(Holiday holi) {
        if (holi.isNew()) {
            holi.setCreateDate(new Date());
        } else {
            holi.setUpdateDate(new Date());
        }
        return holidayRepo.save(holi);
    }

    public boolean isHoliday(Date date) {
        return loadSingle(date) != null;
    }

    public void delete(Holiday holiday) {
        holidayRepo.delete(holiday);
    }

}
