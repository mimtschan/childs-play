package at.qe.sepm.skeleton.services;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Configuration;
import at.qe.sepm.skeleton.model.ConfigurationTimes;
import at.qe.sepm.skeleton.model.temporary.Range;
import at.qe.sepm.skeleton.model.Weekday;
import at.qe.sepm.skeleton.model.temporary.ConfigDayInformation;
import at.qe.sepm.skeleton.model.temporary.DayInformation;
import at.qe.sepm.skeleton.repositories.ConfigurationRepository;

/**
 * @author tade
 *
 */
@Component
@Scope("application")
public class ConfigurationService {

    @Autowired
    private ConfigurationRepository configRepo;
    @Autowired
    private HolidayService holidayService;
    @Autowired
    private ConfigurationTimesService timesService;

    private Configuration singleton;
    private Map<Weekday, ConfigurationTimes> configTimes;

    //Everyone is allowed to load the Config
    public Configuration getConfig() {
        if (singleton == null) {
            singleton = configRepo.findAll().get(0);
            configTimes = new TreeMap<>();
            timesService.getTimeConfigs().forEach((time) -> {
                configTimes.put(time.getId(), time);
            });
        }
        return singleton;
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public Configuration saveConfig(Configuration config) {
        singleton = configRepo.save(config);
        //Save all times if config is saved
        configTimes.values().forEach((time) -> {
            timesService.save(time);
        });
        return singleton;
    }

    @PreAuthorize("hasAuthority('STAFF') or hasAuthority('PARENT')")
    public boolean isOpenOn(Date date) {
        getConfig();
        //call of isOpen() Methods
        // also check whether date is a holiday, closed if yes
        return configTimes.get(Weekday.fromCalendarDay(date)).isOpen() && !holidayService.isHoliday(date);
//		Method isOpen;
//		try {
//			// Dynamic call of isOpenXXXX() Methods (e.g. isOpenMonday())
//			// also check whether date is a holiday, closed if yes
//			isOpen = singleton.getClass().getMethod("isOpen" + mapDateToWeekday(date));
//			return (boolean) isOpen.invoke(singleton) && !holidayService.isHoliday(date);
//		} catch (NoSuchMethodException | SecurityException | IllegalArgumentException | InvocationTargetException
//				| IllegalAccessException e) {
//			return false;
//		}
    }

    public DayInformation getClosedReason(Date date) {
        getConfig();
        DayInformation holiday = holidayService.loadSingle(date);
        if (holiday != null) {
            return holiday;
        }
        Calendar calDate = Calendar.getInstance();
        calDate.setTime(date);
        return new ConfigDayInformation(calDate, configTimes.get(Weekday.fromCalendarDay(date)));
    }

    @PreAuthorize("hasAuthority('STAFF') or hasAuthority('PARENT')")
    public Range<Date> getBringIntervalForDate(Date date) {
        getConfig();
        ConfigurationTimes dayConfig = configTimes.get(Weekday.fromCalendarDay(date));
        return new Range<>(dayConfig.getBringStart(), dayConfig.getBringEnd());
//		return createDateFrom(configTimes.get(Weekday.fromCalendarDay(date)), "Bring");
    }

    @PreAuthorize("hasAuthority('STAFF') or hasAuthority('PARENT')")
    public Range<Date> getGetIntervalForDate(Date date) {
        getConfig();
        ConfigurationTimes dayConfig = configTimes.get(Weekday.fromCalendarDay(date));
        return new Range<>(dayConfig.getGetStart(), dayConfig.getGetEnd());
//		return createDateFrom(configTimes.get(Weekday.fromCalendarDay(date)), "Get");
    }

    @PreAuthorize("hasAuthority('STAFF') or hasAuthority('PARENT')")
    public Map<Weekday, ConfigurationTimes> getAllTimeConfigs() {
        getConfig();
        return configTimes;
    }

    @PreAuthorize("hasAuthority('STAFF') or hasAuthority('PARENT')")
    public Map<Weekday, ConfigurationTimes> getOpenTimeConfigs() {
        getConfig();
        Map<Weekday, ConfigurationTimes> newMap = new TreeMap<>();
        configTimes.forEach((key, value) -> {
            if (value.isOpen()) {
                newMap.put(key, value);
            }
        });
        return newMap;
    }

    @PreAuthorize("hasAuthority('STAFF') or hasAuthority('PARENT')")
    public ConfigurationTimes getForWeekday(Weekday day) {
        getConfig();
        return configTimes.get(day);
    }

    @PreAuthorize("hasAuthority('STAFF') or hasAuthority('PARENT')")
    public ConfigurationTimes getForDate(Date date) {
        getConfig();
        return configTimes.get(Weekday.fromCalendarDay(date));
    }

//	/**
//	 * @param date
//	 * @return Weekday as full String e.g. "Monday"
//	 */
//
//	private Range<Time> createDateFrom(ConfigurationTimes times, String prefix) {
//		Range<Time> range = new Range<>();
//		try {
//			range.setBegin((Time) times.getClass().getMethod("get" + prefix + "Start").invoke(times));
//			range.setEnd((Time) times.getClass().getMethod("get" + prefix + "End").invoke(times));
//		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
//				| SecurityException e) {
//			e.printStackTrace();
//		}
//		return range;
//	}
}
