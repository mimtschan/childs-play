package at.qe.sepm.skeleton.services;

import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.DayException;
import at.qe.sepm.skeleton.model.Parent;
import at.qe.sepm.skeleton.model.Person;
import at.qe.sepm.skeleton.model.temporary.Range;
import at.qe.sepm.skeleton.repositories.DayExceptionRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

@Component
@Scope("application")
public class DayExceptionService {

    @Autowired
    DayExceptionRepository exceptionRepo;
    @Autowired
    UserService userService;
    @Autowired
    ConfigurationService configService;

    @PreAuthorize("hasAuthority('STAFF') or hasAuthority('PARENT')")
    public DayException createDayException() {
        DayException dayException = new DayException();
        dayException.setDate(new Date());
        return dayException;
    }

    @PreAuthorize("hasAuthority('STAFF') or hasAuthority('PARENT')")
    public DayException saveDayException(DayException exception) {
        if (exception.isNew()) {
            exception.setCreateDate(new Date());
            exception.setConfirmed(!isConfirmationNeeded(exception));
            exception.setCreatedBy(userService.getAuthenticatedUser().getPerson());
        } else {
            exception.setUpdateDate(new Date());
        }
        return exceptionRepo.save(exception);
    }

    @PreAuthorize("hasAuthority('STAFF') or hasAuthority('PARENT')")
    public void deleteException(DayException exception) {
        exceptionRepo.delete(exception);
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public DayException confirmException(DayException exception, Person confirmer) {
        exception.setConfirmed(true);
        exception.setConfirmedBy(confirmer);
        exception.setUpdateDate(new Date());
        return exceptionRepo.save(exception);
    }

    @PreAuthorize("hasAuthority('STAFF')or hasAuthority('PARENT')")
    public List<DayException> getAllExceptions() {
        return exceptionRepo.findAll();
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public List<DayException> getAllUnconfirmed() {
        return exceptionRepo.findAllByConfirmedIsFalse();
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public List<DayException> getAllValidForDate(Date date) {
        return exceptionRepo.findAllByDateAndConfirmedIsTrue(date);
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public List<DayException> getAllValidForToday() {
        return getAllValidForDate(new Date());
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public DayException getSingleForChildAndDay(Child child, Date day) {
        return exceptionRepo.findFirstByChildAndDate(child, day);
    }

    @PreAuthorize("hasAuthority('PARENT')")
    public List<DayException> getAllforParent(Parent parent) {
        List<DayException> dayException = new ArrayList<>();
        parent.getChildren().forEach((child) -> {
            dayException.addAll(exceptionRepo.findAllByChild(child));
        });
        return dayException;
    }

    public DayException getSingle(Long dayExceptionId) {
        return exceptionRepo.findOne(dayExceptionId);
    }

    private boolean isConfirmationNeeded(DayException exception) {
        //Confirmation of Staff is only needed, if absent, alternative Getter, or bring/getting 
        //time differs from defaults
        Range<Date> bringRange = configService.getBringIntervalForDate(exception.getDate());
        Range<Date> getRange = configService.getGetIntervalForDate(exception.getDate());

        return (exception.isAbsent()) || (exception.getAlternateGetter() != null)
                || !bringRange.isInRange(exception.getAlternateBringTime()) || !getRange.isInRange(exception.getAlternateGetTime());
    }

}
