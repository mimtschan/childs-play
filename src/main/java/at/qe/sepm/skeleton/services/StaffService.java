package at.qe.sepm.skeleton.services;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Staff;
import at.qe.sepm.skeleton.model.UserRole;
import at.qe.sepm.skeleton.repositories.StaffRepository;

@Component
@Scope("application")
public class StaffService extends UserBasedService<Staff> {

    @Autowired
    StaffRepository staffRepo;

    @PreAuthorize("hasAuthority('STAFF')")
    public List<Staff> getAllStaff() {
        return staffRepo.findAll();
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public Staff createStaff() {
        return createPerson();
    }

    @Override
    protected Staff instantiate() {
        return new Staff();
    }

    @Override
    protected void initBasicRoles(Set<UserRole> roles) {
        roles.add(UserRole.STAFF);
    }

}
