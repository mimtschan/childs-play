package at.qe.sepm.skeleton.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.ChildReferencePersonRelation;
import at.qe.sepm.skeleton.model.ReferencePerson;
import at.qe.sepm.skeleton.repositories.ChildRefPersonRelationRepository;
import java.util.ArrayList;
import java.util.Date;
import org.springframework.security.access.prepost.PreAuthorize;

@Component
@Scope("application")
public class ChildRefPersonRelationService {

    @Autowired
    private ChildRefPersonRelationRepository relationRepo;

    /**
     * Get all reference person relations for a child
     *
     * @param child
     * @return List of relations
     */
    @PreAuthorize("hasAuthority('STAFF') or hasAuthority('PARENT')")
    public List<ChildReferencePersonRelation> getRelationForChild(Child child) {
        return relationRepo.findByChild(child);
    }

    /**
     * Get all reference person relations for list of children
     *
     * @param children list of children
     * @return List of relations
     */
    @PreAuthorize("hasAuthority('STAFF') or hasAuthority('PARENT')")
    public List<ChildReferencePersonRelation> getRelationForChildren(List<Child> children) {
        List<ChildReferencePersonRelation> retList = new ArrayList<>();
        children.forEach((child) -> {
            retList.addAll(getRelationForChild(child));
        });
        return retList;
    }

    /**
     * Get all relations for a reference person
     *
     * @param person
     * @return List of relations
     */
    @PreAuthorize("hasAuthority('STAFF') or hasAuthority('PARENT')")
    public List<ChildReferencePersonRelation> getRelationForReferencePerson(ReferencePerson person) {
        return relationRepo.findByReferencePerson(person);
    }

    /**
     * Create a new relation
     *
     * @return new relation instance
     */
    @PreAuthorize("hasAuthority('STAFF') or hasAuthority('PARENT')")
    public ChildReferencePersonRelation createRelation() {
        return new ChildReferencePersonRelation();
    }

    /**
     *
     * @param relation
     * @return changed relation during save
     */
    @PreAuthorize("hasAuthority('STAFF') or hasAuthority('PARENT')")
    public ChildReferencePersonRelation saveRelation(ChildReferencePersonRelation relation) {
        if (relation.isNew()) {
            relation.setCreateDate(new Date());
        } else {
            relation.setUpdateDate(new Date());
        }
        return relationRepo.save(relation);
    }

    /**
     * Returns all relations in the system
     *
     * @return relation list
     */
    @PreAuthorize("hasAuthority('STAFF')")
    public List<ChildReferencePersonRelation> getAllRelations() {
        return relationRepo.findAll();
    }

    /**
     * Read one specific relation
     *
     * @param id
     * @return relation instance
     */
    @PreAuthorize("hasAuthority('STAFF') or hasAuthority('PARENT')")
    public ChildReferencePersonRelation getSingle(long id) {
        return relationRepo.findOne(id);
    }

    /**
     * delete a given relation
     *
     * @param relation
     */
    @PreAuthorize("hasAuthority('STAFF') or hasAuthority('PARENT')")
    public void deleteRelation(ChildReferencePersonRelation relation) {
        relationRepo.delete(relation);
    }

}
