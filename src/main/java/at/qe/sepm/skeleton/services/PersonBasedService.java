package at.qe.sepm.skeleton.services;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import at.qe.sepm.skeleton.model.Gender;
import at.qe.sepm.skeleton.model.Person;
import at.qe.sepm.skeleton.repositories.PersonRepository;

/**
 * This Basic Service implements all basic function for all Person based
 * entities
 *
 * @author tade
 * @param <T> Specific person entity
 */
public abstract class PersonBasedService<T extends Person> {

    @Autowired
    protected PersonRepository<T> personRepo;
    @Autowired
    protected UserService userService;

    public void deletePerson(T person) {
        prepareDeletion(person);
        personRepo.delete(person);
    }

    /**
     * Saves a person
     *
     * @param person
     * @return
     */
    public T savePerson(T person) {
        if (person.isNew()) {
            person.setCreateDate(new Date());
            //Created user may be setted, because 
            //staff can create reference persons for a parent
            if (person.getCreateUser() == null) {
                person.setCreateUser(userService.getAuthenticatedUser().getPerson());
            }
        } else {
            person.setUpdateDate(new Date());
            person.setUpdateUser(userService.getAuthenticatedUser().getPerson());
        }
        return personRepo.save(person);
    }

    public T loadPersonById(Long id) {
        return personRepo.findOne(id);
    }

    /**
     *
     * @return List of all Genders
     */
    public List<Gender> getAllGenders() {
        return Arrays.asList(Gender.values());
    }

    /**
     * Creates a new Instance with the basic setup
     *
     * @return fully initialized instance of specific person
     */
    protected T createPerson() {
        T newPerson = instantiate();
        newPerson.setGender(Gender.UNKNOWN);
        newPerson.setEnabled(true);
        return newPerson;
    }

    /**
     * create a new Instance of the specific person type
     *
     * @return new specific Instance
     */
    protected abstract T instantiate();

    /**
     * Gets all Person of a type T where a given person created this persons
     *
     * @param person creator of the persons
     * @return List of specific Persons
     */
    public List<T> getCreatedBy(Person person) {
        return personRepo.findAllByCreateUser(person);
    }

    /**
     * Gets all Person of a type T where the persons are enabled and a given
     * person created this persons
     *
     * @param person creator of the persons
     * @return List of specific Persons
     */
    public List<T> getCreatedByAndIsActive(Person person) {
        return personRepo.findAllByCreateUserAndEnabledIsTrue(person);
    }

    /**
     * toggles the enable state of a person
     *
     * @param person
     */
    public void toggleEnable(T person) {
        person.setEnabled(!person.isEnabled());
    }

    /**
     * actions need to be done, for deletion
     *
     * @param person person to delete
     */
    protected void prepareDeletion(T person) {
        //redefine in Subclasses if needed
    }

}
