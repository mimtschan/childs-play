package at.qe.sepm.skeleton.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.Parent;
import at.qe.sepm.skeleton.model.UserRole;
import at.qe.sepm.skeleton.repositories.ParentRepository;

@Component
@Scope("application")
public class ParentService extends UserBasedService<Parent> {

    @Autowired
    private ParentRepository parentRepo;
    @Autowired
    private ReferencePersonService refPersonService;

    @PreAuthorize("hasAuthority('STAFF') or principal.username eq #parent.getUser().getUsername()")
    public List<Child> getChildrenForParent(Parent parent) {
        return parent.getChildren();
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public Parent createParent() {
        return createPerson();
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public List<Parent> getAllParents() {
        return parentRepo.findAll();
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public List<Parent> getAllInactiveParents() {
        List<Parent> parents = new ArrayList<>();
        userService.getInactiveParentUsers().forEach((user) -> {
            parents.add((Parent) user.getPerson());
        });
        return parents;
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public List<Parent> getAllActiveParents() {
        List<Parent> parents = new ArrayList<>();
        userService.getActiveParentUsers().forEach((user) -> {
            parents.add((Parent) user.getPerson());
        });
        return parents;
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public boolean inactivateParent(Parent parent) {
        Set<UserRole> authorities = parent.getUser().getRoles();
        if (authorities.contains(UserRole.PARENT)) {
            authorities.remove(UserRole.PARENT);
            return true;
        }
        return false;
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public boolean activateParent(Parent parent) {
        Set<UserRole> authorities = parent.getUser().getRoles();
        if (!authorities.contains(UserRole.PARENT)) {
            authorities.add(UserRole.PARENT);
            return true;
        }
        return false;
    }

    @Override
    protected Parent instantiate() {
        return new Parent();
    }

    @Override
    protected void initBasicRoles(Set<UserRole> roles) {
        //All new parents get both parent roles
        roles.add(UserRole.INACTPARENT);
        roles.add(UserRole.PARENT);
    }

    @Override
    protected void prepareDeletion(Parent person) {
        //delete all Rerefence Persons created by Parent, if parent will be deleted
        refPersonService.getCreatedBy(person).forEach((refPerson) -> refPersonService.deletePerson(refPerson));
    }

}
