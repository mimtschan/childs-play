package at.qe.sepm.skeleton.services;

import at.qe.sepm.skeleton.model.DayException;
import at.qe.sepm.skeleton.model.Person;
import at.qe.sepm.skeleton.model.ReferencePerson;
import at.qe.sepm.skeleton.model.Staff;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.temporary.Confirmation;
import at.qe.sepm.skeleton.model.temporary.DayExceptionConfirmation;
import at.qe.sepm.skeleton.model.temporary.ReferencePersonConfirmation;
import at.qe.sepm.skeleton.notification.EmailService;

@Component
@Scope("application")
public class ConfirmationService {

    @Autowired
    private ReferencePersonService refPersonService;
    @Autowired
    private DayExceptionService dayExceptionService;
    @Autowired
    private EmailService emailService;

    @PreAuthorize("hasAuthority('STAFF')")
    public List<Confirmation> getAllConfirmations() {
        List<Confirmation> returnList = new ArrayList<>();
        refPersonService.getAllUnconfirmedReferencePerson()
                .forEach((refPerson) -> returnList.add(createByRefPerson(refPerson)));
        dayExceptionService.getAllUnconfirmed()
                .forEach((exception) -> returnList.add(createByException(exception)));
        return returnList;
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public List<ReferencePersonConfirmation> getRefPersonConfirmations() {
        List<ReferencePersonConfirmation> returnList = new ArrayList<>();
        refPersonService.getAllUnconfirmedReferencePerson()
                .forEach((refPerson) -> returnList.add(createByRefPerson(refPerson)));
        return returnList;
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public List<DayExceptionConfirmation> getDayExceptionConfirmations() {
        List<DayExceptionConfirmation> returnList = new ArrayList<>();
        dayExceptionService.getAllUnconfirmed()
                .forEach((exception) -> returnList.add(createByException(exception)));
        return returnList;
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public void confirm(Confirmation confirmation, Person confirmer) {
        confirmation.confirm((Staff) confirmer);
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public void decline(Confirmation confirmation, Person decliner) {
        confirmation.decline((Staff) decliner);
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public DayExceptionConfirmation createByException(DayException exception) {
        return new DayExceptionConfirmation(dayExceptionService, exception, emailService);
    }

    @PreAuthorize("hasAuthority('STAFF')")
    public ReferencePersonConfirmation createByRefPerson(ReferencePerson refPerson) {
        return new ReferencePersonConfirmation(refPersonService, refPerson, emailService);
    }

}
