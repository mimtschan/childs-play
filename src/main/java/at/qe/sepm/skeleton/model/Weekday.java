package at.qe.sepm.skeleton.model;

import java.util.Calendar;
import java.util.Date;

/**
 * Enumeration of available user roles.
 */
public enum Weekday {

    MONDAY("Montag"), TUESDAY("Dienstag"), WEDNESDAY("Mittwoch"), THURSDAY("Donnerstag"), FRIDAY("Freitag"), SATURDAY(
            "Samstag"), SUNDAY("Sonntag");

    private final String text;

    private Weekday(String text) {
        this.text = text;
    }

    /**
     * Fast method to convert Weekday Enum to constant value of Calendar
     *
     * @return Value of fitting Calendar constant
     */
    public int toCalendarWeekday() {
        try {
            return Calendar.class.getField(this.name()).getInt(null);
        } catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
            return 99;
        }
    }

    /**
     * Easy method to create instance of WeekdayEnum from a Calendar weekday
     * value
     *
     * @param weekday Value of Calendar Constant
     * @return fitting Weekday instance
     */
    public static Weekday fromCalendarWeekday(int weekday) {
        switch (weekday) {
            case Calendar.MONDAY:
                return MONDAY;
            case Calendar.TUESDAY:
                return TUESDAY;
            case Calendar.WEDNESDAY:
                return WEDNESDAY;
            case Calendar.THURSDAY:
                return THURSDAY;
            case Calendar.FRIDAY:
                return FRIDAY;
            case Calendar.SATURDAY:
                return SATURDAY;
        }
        return SUNDAY;
    }

    /**
     * Easy method to create instance of WeekdayEnum from Date instance
     *
     * @param day Date instance
     * @return fitting Weekday Instance
     */
    public static Weekday fromCalendarDay(Date day) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(day);
        return fromCalendar(cal);
    }

    /**
     * Easy method to create instance of WeekdayEnum from Calendar instance
     *
     * @param day Calendar instance
     * @return fitting Weekday Instance
     */
    public static Weekday fromCalendar(Calendar day) {
        return fromCalendarWeekday(day.get(Calendar.DAY_OF_WEEK));
    }

    @Override
    public String toString() {
        return this.text;
    }

}
