package at.qe.sepm.skeleton.model;

import javax.persistence.Entity;

/**
 * Entity representing a staff member.
 */
@Entity
public class Staff extends Person {

    private static final long serialVersionUID = 1L;

}
