package at.qe.sepm.skeleton.model;

import java.util.List;
import javax.persistence.CascadeType;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * Entity representing Parent.
 */
@Entity
public class Parent extends Person {

    @ManyToMany(mappedBy = "parents")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Child> children;

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "assigned")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Task> assignedTasks;

    private static final long serialVersionUID = 1L;

    /**
     * @return the children
     */
    public List<Child> getChildren() {
        return children;
    }

    public void setChildren(List<Child> children) {
        this.children = children;
    }

    /**
     * @return the assignedTasks
     */
    public List<Task> getAssignedTasks() {
        return assignedTasks;
    }

    /**
     * @param assignedTasks the assignedTasks to set
     */
    public void setAssignedTasks(List<Task> assignedTasks) {
        this.assignedTasks = assignedTasks;
    }

}
