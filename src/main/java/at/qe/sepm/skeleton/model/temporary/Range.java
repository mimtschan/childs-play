package at.qe.sepm.skeleton.model.temporary;

/**
 * This class is holds a pair instance of a given class. It can checks whether a
 * third instance is between the pair or not
 *
 * @author tade
 * @param <T> Type of the classes of the pair
 */
public final class Range<T extends Comparable<? super T>> {

    private T begin;
    private T end;

    /**
     * Empty constructor
     */
    public Range() {
    }

    /**
     * Create and initially set the pair
     *
     * @param begin start of the range
     * @param end end of the range
     */
    public Range(T begin, T end) {
        this.setBegin(begin);
        this.setEnd(end);
    }

    /**
     * @return the start
     */
    public T getBegin() {
        return begin;
    }

    /**
     * @param begin the start to set
     */
    public void setBegin(T begin) {
        this.begin = begin;
    }

    /**
     * @return the end
     */
    public T getEnd() {
        return end;
    }

    /**
     * @param end the end to set
     */
    public void setEnd(T end) {
        this.end = end;
    }

    /**
     * Checks if a third instance is between the start and end instance Between
     * also includes the borders
     *
     * @param value third instance
     * @return True if in range, false if out of range
     */
    public boolean isInRange(T value) {
        if (value == null) {
            return false;
        }
        return (begin.compareTo(value) <= 0) && (end.compareTo(value) >= 0);
    }

}
