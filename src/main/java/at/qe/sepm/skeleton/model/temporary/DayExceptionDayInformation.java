package at.qe.sepm.skeleton.model.temporary;

import java.util.Calendar;
import java.util.Date;

import at.qe.sepm.skeleton.model.ConfigurationTimes;
import at.qe.sepm.skeleton.model.DayException;
import at.qe.sepm.skeleton.model.Person;

/**
 * This day information represents a special exception, created by a parent for
 * a specific day and a child. It extends the ConfigDayInformation because, not
 * all fields are mandatory, so missing fields will be filled by the
 * configuration information
 *
 * @author tade
 */
public class DayExceptionDayInformation extends ConfigDayInformation {

    private final DayException dayException;

    /**
     *
     * @param day Date information
     * @param config Weekday configuration information
     * @param dayException specific exception information
     */
    public DayExceptionDayInformation(Calendar day, ConfigurationTimes config, DayException dayException) {
        super(day, config);
        this.dayException = dayException;
    }

    @Override
    public Date getStart() {
        //if parent set alternate BringTime use this time
        Date time = dayException.getAlternateBringTime();
        if (time == null) {
            return super.getStart();
        }
        //weekday configuration otherwise
        return mergeDateWithTime(day, time);
    }

    @Override
    public Date getEnd() {
        //if parent set alternate GetTime use this time
        Date time = dayException.getAlternateGetTime();
        if (time == null) {
            return super.getEnd();
        }
        //weekday configuration otherwise
        return mergeDateWithTime(day, time);
    }

    @Override
    public String getText() {
        //Description is always text
        String descr = dayException.getDescription();
        //optional add AlternateGetter if set
        Person getter = dayException.getAlternateGetter();
        if (getter != null) {
            return descr + " Abholer: " + getter;
        }
        return descr;
    }

    @Override
    public String getColorClass() {
        if (dayException.isAbsent()) {
            return DayInformation.RED;
        }
        return DayInformation.YELLOW;
    }

}
