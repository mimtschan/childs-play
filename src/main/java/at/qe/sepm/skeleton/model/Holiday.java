package at.qe.sepm.skeleton.model;

import java.util.Date;
import java.util.Calendar;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.domain.Persistable;

import at.qe.sepm.skeleton.model.temporary.DayInformation;

/**
 * Entity representing a Holiday where the nursery is closed.
 */
@Entity
public class Holiday implements Persistable<Date>, DayInformation {

    private static final long serialVersionUID = 1L;

    @Id
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date date;

    private String description;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    @Override
    public Date getId() {
        return date;
    }

    @Override
    public boolean isNew() {
        return (null == getCreateDate());
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the createDate
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the updateDate
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.getDate());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Holiday)) {
            return false;
        }
        final Holiday other = (Holiday) obj;
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        return true;
    }

    @Override
    public Date getStart() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(this.date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();
    }

    @Override
    public Date getEnd() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(this.date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        return cal.getTime();
    }

    @Override
    public String getText() {
        return this.getDescription();
    }

    @Override
    public String getColorClass() {
        return DayInformation.RED;
    }
}
