package at.qe.sepm.skeleton.model.temporary;

import java.util.Calendar;
import java.util.Date;

/**
 * This abstract base class implements basic functionality, when the day
 * information is created by a specific day.
 *
 * @author tade
 */
public abstract class DayInformationBase implements DayInformation {

    protected Calendar day;

    /**
     *
     * @param day is the day for the entry in the Calendar
     */
    public DayInformationBase(Calendar day) {
        this.day = day;
    }

    @Override
    public Date getStart() {
        //Basic implementation - Start is always at 00:00:00
        setTimeInCal(day, 0, 0, 0);
        return day.getTime();
    }

    @Override
    public Date getEnd() {
        //Basic implementation - End is always at 23:59:59
        setTimeInCal(day, 23, 59, 59);
        return day.getTime();
    }

    /**
     * This helpermethod merges the time of a date object into the calendar
     * object, but leaves the date of the calendar untouched
     *
     * @param date Calendar which is the base of the date
     * @param time Date-Object which time is merged into the date object
     * @return merged Date object
     */
    protected Date mergeDateWithTime(Calendar date, Date time) {
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(time);
        setDateInCal(cal2, date.get(Calendar.DATE), date.get(Calendar.MONTH), date.get(Calendar.YEAR));
        return cal2.getTime();
    }

    /**
     * Helper method to set the time easier in a calendar object
     *
     * @param date Calendar object
     * @param hour hour for setting time
     * @param minute minute for setting time
     * @param second seconds for setting time
     */
    public static void setTimeInCal(Calendar date, int hour, int minute, int second) {
        date.set(Calendar.HOUR_OF_DAY, hour);
        date.set(Calendar.MINUTE, minute);
        date.set(Calendar.SECOND, second);
    }

    /**
     * Helper method to set the date easier in a calendar object whichout
     * changing the time
     *
     * @param date Calendar object
     * @param day Day in month
     * @param month Month consant
     * @param year year
     */
    public static void setDateInCal(Calendar date, int day, int month, int year) {
        date.set(Calendar.DATE, day);
        date.set(Calendar.MONTH, month);
        date.set(Calendar.YEAR, year);
    }

}
