package at.qe.sepm.skeleton.model.temporary;

import java.util.Date;

/**
 * This Interface represents a Information for a Day for a Child for the
 * Calendar
 *
 * @author tade
 */
public interface DayInformation {

    final static String RED = "red";
    final static String GREEN = "green";
    final static String YELLOW = "yellow";

    /**
     *
     * @return The Start Date and Time of this timeblock for Calendar
     */
    public Date getStart();

    /**
     *
     * @return The End Date and Time of this timeblock for Calendar
     */
    public Date getEnd();

    /**
     *
     * @return The Text of this timeblock for Calendar
     */
    public String getText();

    /**
     *
     * @return The Color of this timeblock for Calendar
     */
    public String getColorClass();

}
