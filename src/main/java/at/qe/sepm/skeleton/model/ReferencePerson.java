package at.qe.sepm.skeleton.model;

import java.util.List;
import javax.persistence.CascadeType;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * Entity representing Reference Persons for children.
 */
@Entity
public class ReferencePerson extends Person {

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "referencePerson")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<ChildReferencePersonRelation> reference;

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "alternateGetter")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<DayException> dayException;

    private static final long serialVersionUID = 1L;

    /**
     * @return the referenceChildren
     */
    public List<ChildReferencePersonRelation> getReference() {
        return reference;
    }

    /**
     * @return
     */
    public List<DayException> getDayException() {
        return dayException;
    }

    /**
     * @param reference the reference to set
     */
    public void setReference(List<ChildReferencePersonRelation> reference) {
        this.reference = reference;
    }

    /**
     * @param dayException the dayException to set
     */
    public void setDayException(List<DayException> dayException) {
        this.dayException = dayException;
    }

}
