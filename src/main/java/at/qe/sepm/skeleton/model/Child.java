package at.qe.sepm.skeleton.model;

import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * Entity representing Child
 */
@Entity
public class Child extends Person {

    private static final long serialVersionUID = 1L;

    private String allergies;

    private String comments;

    @ManyToOne
    private Person emergencyContact;

    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Child> siblings;

    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Parent> parents;

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "child")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<ChildReferencePersonRelation> reference;

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "child")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<DayException> dayExceptions;

    @ElementCollection(targetClass = Weekday.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "Child_Weekday")
    private Set<Weekday> subDates;

    @Temporal(TemporalType.DATE)
    private Date registrationDate;

    @Temporal(TemporalType.DATE)
    private Date deRegistrationDate;

    /**
     * @return the allergies
     */
    public String getAllergies() {
        return allergies;
    }

    /**
     * @param allergies the allergies to set
     */
    public void setAllergies(String allergies) {
        this.allergies = allergies;
    }

    /**
     * @return the comments
     */
    public String getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * @return the siblings
     */
    public List<Child> getSiblings() {
        return siblings;
    }

    /**
     * @param siblings the siblings to set
     */
    public void setSiblings(List<Child> siblings) {
        this.siblings = siblings;
    }

    /**
     * @return the parents
     */
    public List<Parent> getParents() {
        return parents;
    }

    /**
     * @param parents the parents to set
     */
    public void setParents(List<Parent> parents) {
        this.parents = parents;
    }

    /**
     * @return the subDates
     */
    public Set<Weekday> getSubDates() {
        return subDates;
    }

    /**
     * @param subDates the subDates to set
     */
    public void setSubDates(Set<Weekday> subDates) {
        this.subDates = subDates;
    }

    /**
     * @return the emergencyContact
     */
    public Person getEmergencyContact() {
        return emergencyContact;
    }

    /**
     * @param emergencyContact the emergencyContact to set
     */
    public void setEmergencyContact(Person emergencyContact) {
        this.emergencyContact = emergencyContact;
    }

    /**
     * @return the referencePersons
     */
    public List<ChildReferencePersonRelation> getReference() {
        return reference;
    }

    /**
     * @param reference the referencePersons to set
     */
    public void setReference(List<ChildReferencePersonRelation> reference) {
        this.reference = reference;
    }

    /**
     * @return the registrationDate
     */
    public Date getRegistrationDate() {
        return registrationDate;
    }

    /**
     * @param registrationDate the registrationDate to set
     */
    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    /**
     * @return the deRegistrationDate
     */
    public Date getDeRegistrationDate() {
        return deRegistrationDate;
    }

    /**
     * @param deRegistrationDate the deRegistrationDate to set
     */
    public void setDeRegistrationDate(Date deRegistrationDate) {
        this.deRegistrationDate = deRegistrationDate;
    }

    public String getParentsAsString() {
        StringBuilder builder = new StringBuilder();
        this.getParents().forEach((Parent parent) -> {
            if (builder.length() != 0) {
                builder.append("; ");
            }
            builder.append(parent);
        });
        return builder.toString();
    }

    /**
     * @return the dayExceptions
     */
    public List<DayException> getDayExceptions() {
        return dayExceptions;
    }

    /**
     * @param dayExceptions the dayExceptions to set
     */
    public void setDayExceptions(List<DayException> dayExceptions) {
        this.dayExceptions = dayExceptions;
    }

}
