package at.qe.sepm.skeleton.model.temporary;

import at.qe.sepm.skeleton.model.DayException;
import at.qe.sepm.skeleton.model.Staff;
import at.qe.sepm.skeleton.notification.EmailService;
import at.qe.sepm.skeleton.services.DayExceptionService;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * This class represents a day exception a parent created for a specific day for
 * his child. Either the exception is ok, and will be activated or deleted if
 * not ok for the staff.
 *
 * @author tade
 */
public class DayExceptionConfirmation implements Confirmation {

    private final EmailService emailService;
    private final DayExceptionService service;
    private DayException toConfirm;

    private final DateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");

    /**
     *
     * @param service DayExceptionService because autowire does not work
     * @param toConfirm the dayException which should be checked by staff
     * @param emailService Email service - maybe a notification will be send
     */
    public DayExceptionConfirmation(DayExceptionService service, DayException toConfirm, EmailService emailService) {
        this.service = service;
        this.toConfirm = toConfirm;
        this.emailService = emailService;
    }

    @Override
    public void confirm(Staff confirmer) {
        service.confirmException(toConfirm, confirmer);
        toConfirm = service.saveDayException(toConfirm);
    }

    @Override
    public String getText() {
        return dateFormatter.format(toConfirm.getDate()) + " " + toConfirm.getChild() + " " + toConfirm.getDescription();
    }

    @Override
    public String getTypeText() {
        return "Tagesausnahme";
    }

    @Override
    public void decline(Staff decliner) {
        //Send email on decline, because exception will be instant deleted
        emailService.sendSimpleMessage(this, decliner);
        service.deleteException(toConfirm);
    }

    @Override
    public String getCreatorsEmail() {
        //parent's email
        return toConfirm.getCreatedBy().getEmail();
    }

}
