package at.qe.sepm.skeleton.model;

/**
 * Enumeration of available user roles.
 */
public enum Gender {

    UNKNOWN("unbekannt"), MALE("männlich"), FEMALE("weiblich");

    private final String name;

    private Gender(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }

}
