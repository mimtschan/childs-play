package at.qe.sepm.skeleton.model;

import java.util.Date;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.data.domain.Persistable;

/**
 * This entity represents the relation of a child and a reference person
 *
 * @author tade
 */
@Entity
public class ChildReferencePersonRelation implements Persistable<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long relationId;

    @ManyToOne
    private Child child;

    @ManyToOne
    private ReferencePerson referencePerson;

    private String relation;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.getRelationId());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof ChildReferencePersonRelation)) {
            return false;
        }
        final ChildReferencePersonRelation other = (ChildReferencePersonRelation) obj;
        if (!Objects.equals(this.relationId, other.relationId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "at.qe.sepm.skeleton.model.ChildReferencePersonRelation[ id=" + getRelationId() + " ]";
    }

    @Override
    public Long getId() {
        return relationId;
    }

    @Override
    public boolean isNew() {
        return (null == getCreateDate());
    }

    /**
     * @return the relationId
     */
    public Long getRelationId() {
        return relationId;
    }

    /**
     * @param relationId the relationId to set
     */
    public void setRelationId(Long relationId) {
        this.relationId = relationId;
    }

    /**
     * @return the child
     */
    public Child getChild() {
        return child;
    }

    /**
     * @param child the child to set
     */
    public void setChild(Child child) {
        this.child = child;
    }

    /**
     * @return the referencePerson
     */
    public ReferencePerson getReferencePerson() {
        return referencePerson;
    }

    /**
     * @param referencePerson the referencePerson to set
     */
    public void setReferencePerson(ReferencePerson referencePerson) {
        this.referencePerson = referencePerson;
    }

    /**
     * @return the relation
     */
    public String getRelation() {
        return relation;
    }

    /**
     * @param relation the relation to set
     */
    public void setRelation(String relation) {
        this.relation = relation;
    }

    /**
     * @return the createDate
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the updateDate
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

}
