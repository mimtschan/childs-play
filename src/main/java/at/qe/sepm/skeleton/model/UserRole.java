package at.qe.sepm.skeleton.model;

/**
 * Enumeration of available user roles.
 */
public enum UserRole {

    ADMIN,
    STAFF,
    PARENT,
    INACTPARENT;

}
