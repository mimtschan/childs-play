package at.qe.sepm.skeleton.model.temporary;

import at.qe.sepm.skeleton.model.ReferencePerson;
import at.qe.sepm.skeleton.model.Staff;
import at.qe.sepm.skeleton.notification.EmailService;
import at.qe.sepm.skeleton.services.ReferencePersonService;

/**
 * This class represents a new Reference Person created by parent. This new
 * Reference Person must be confirmed or deleted by a staff member
 *
 * @author tade
 */
public class ReferencePersonConfirmation implements Confirmation {

    private final EmailService emailService;
    private final ReferencePersonService service;
    private ReferencePerson toConfirm;

    /**
     *
     * @param service Service for Reference Person is needed because autowire
     * does not work in self created objects
     * @param toConfirm Reference Person which should be confirmed
     * @param emailService Service for notification, also no autowire possible
     */
    public ReferencePersonConfirmation(ReferencePersonService service, ReferencePerson toConfirm, EmailService emailService) {
        this.service = service;
        this.toConfirm = toConfirm;
        this.emailService = emailService;
    }

    @Override
    public String getText() {
        return toConfirm + " von: " + toConfirm.getCreateUser();
    }

    @Override
    public void confirm(Staff confirmer) {
        //confirm with serivce and save. no mail is send
        service.confirmReferencePerson(toConfirm);
        toConfirm = service.savePerson(toConfirm);
    }

    @Override
    public String getTypeText() {
        return "neue Bezugsperson";
    }

    @Override
    public void decline(Staff decliner) {
        //at decline send a mail to the creator, because the 
        //reference person will be deleted
        emailService.sendSimpleMessage(this, decliner);
        service.deletePerson(toConfirm);
    }

    @Override
    public String getCreatorsEmail() {
        //E-Mail of the parent
        return toConfirm.getCreateUser().getEmail();
    }
}
