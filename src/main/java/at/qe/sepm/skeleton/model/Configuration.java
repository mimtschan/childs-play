package at.qe.sepm.skeleton.model;

import java.sql.Time;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.data.domain.Persistable;

/**
 * Entity representing Base Configuration of the Application.
 */
@Entity
public class Configuration implements Persistable<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long configId;

    private String name;
    private String logoUrl;
    private String theme;
    private Weekday lunchDeadlineDay;
    private Time lunchDeadlineTime;

    @Override
    public Long getId() {
        return configId;
    }

    @Override
    public boolean isNew() {
        return configId == null;
    }

    public Long getConfigId() {
        return configId;
    }

    public void setConfigId(Long lunchId) {
        this.configId = lunchId;
    }

    /**
     * @return the lunchDeadlineDay
     */
    public Weekday getLunchDeadlineDay() {
        return lunchDeadlineDay;
    }

    /**
     * @param lunchDeadlineDay the lunchDeadlineDay to set
     */
    public void setLunchDeadlineDay(Weekday lunchDeadlineDay) {
        this.lunchDeadlineDay = lunchDeadlineDay;
    }

    /**
     * @return the lunchDeadlineTime
     */
    public Time getLunchDeadlineTime() {
        return lunchDeadlineTime;
    }

    /**
     * @param lunchDeadlineTime the lunchDeadlineTime to set
     */
    public void setLunchDeadlineTime(Time lunchDeadlineTime) {
        this.lunchDeadlineTime = lunchDeadlineTime;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the logoUrl
     */
    public String getLogoUrl() {
        return logoUrl;
    }

    /**
     * @param logoUrl the logoUrl to set
     */
    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    /**
     * @return the theme
     */
    public String getTheme() {
        return theme;
    }

    /**
     * @param theme the theme to set
     */
    public void setTheme(String theme) {
        this.theme = theme;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.getConfigId());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Configuration)) {
            return false;
        }
        final Configuration other = (Configuration) obj;
        if (!Objects.equals(this.configId, other.configId)) {
            return false;
        }
        return true;
    }

}
