package at.qe.sepm.skeleton.model.temporary;

import at.qe.sepm.skeleton.model.Staff;

/**
 * This interface represents a confirmation, which must be checked by the staff.
 * The staff can either confirm or decline it.
 *
 * @author tade
 */
public interface Confirmation {

    /**
     *
     * @return Some general text, which specifies the type of the confirmation
     */
    public String getTypeText();

    /**
     *
     * @return Some specific text, on which the staff should get enough
     * information to confirm or decline the confirmation
     */
    public String getText();

    /**
     * This method confirms the given confirmation
     *
     * @param confirmer The confirmer may be saved in the confirmation
     */
    public void confirm(Staff confirmer);

    /**
     * This method declines the given confirmation
     *
     * @param decliner The confirmer may be mentioned in the notification
     */
    public void decline(Staff decliner);

    /**
     * The Creator of the confirmation may gets information
     *
     * @return The email of the creator of the confirmation
     */
    public String getCreatorsEmail();

}
