package at.qe.sepm.skeleton.model.temporary;

import java.util.Calendar;
import java.util.Date;

import at.qe.sepm.skeleton.model.ConfigurationTimes;

/**
 * This Class represents a Infomation for a day, which merges a given date which
 * a corresponding information for a weekday. This is necessary, because a
 * configuration information is for a weekday, but for the calendar a
 * dateinformation is also needed
 *
 * @author tade
 */
public class ConfigDayInformation extends DayInformationBase {

    private final ConfigurationTimes dayConfig;

    /**
     *
     * @param day Calendarobject for the date information
     * @param config Configuration for a specific weekday
     */
    public ConfigDayInformation(Calendar day, ConfigurationTimes config) {
        super(day);
        this.dayConfig = config;
    }

    @Override
    public Date getStart() {
        //if the config says is open, use time of the configuration
        if (this.dayConfig.isOpen()) {
            return this.mergeDateWithTime(this.day, this.dayConfig.getBringStart());
        }
        return super.getStart();
    }

    @Override
    public Date getEnd() {
        //if the config says is open, use time of the configuration
        if (this.dayConfig.isOpen()) {
            return this.mergeDateWithTime(this.day, this.dayConfig.getGetEnd());
        }
        return super.getEnd();
    }

    @Override
    public String getText() {
        if (this.dayConfig.isOpen()) {
            return "anwesend";
        }
        return "geschlossen";
    }

    @Override
    public String getColorClass() {
        if (this.dayConfig.isOpen()) {
            return DayInformation.GREEN;
        }
        return DayInformation.RED;
    }

}
