package at.qe.sepm.skeleton.model;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.domain.Persistable;

/**
 * Entity representing Configuration for one weekday of the Application.
 */
@Entity
public class ConfigurationTimes implements Persistable<Weekday> {

    private static final long serialVersionUID = 1L;

    @Id
    private Weekday day;

    private boolean open;

    @Temporal(TemporalType.TIME)
    private Date BringStart;
    @Temporal(TemporalType.TIME)
    private Date BringEnd;
    @Temporal(TemporalType.TIME)
    private Date GetStart;
    @Temporal(TemporalType.TIME)
    private Date GetEnd;
    private Integer childCapacity;

    @Override
    public Weekday getId() {
        return day;
    }

    @Override
    public boolean isNew() {
        return false;
    }

    public Weekday getDay() {
        return day;
    }

    public void setDay(Weekday day) {
        this.day = day;
    }

    /**
     * @return the open
     */
    public boolean isOpen() {
        return open;
    }

    /**
     * @param open the open to set
     */
    public void setOpen(boolean open) {
        this.open = open;
    }

    /**
     * @return the BringStart
     */
    public Date getBringStart() {
        return BringStart;
    }

    /**
     * @param BringStart the BringStart to set
     */
    public void setBringStart(Date BringStart) {
        this.BringStart = BringStart;
    }

    /**
     * @return the BringEnd
     */
    public Date getBringEnd() {
        return BringEnd;
    }

    /**
     * @param BringEnd the BringEnd to set
     */
    public void setBringEnd(Date BringEnd) {
        this.BringEnd = BringEnd;
    }

    /**
     * @return the GetStart
     */
    public Date getGetStart() {
        return GetStart;
    }

    /**
     * @param GetStart the GetStart to set
     */
    public void setGetStart(Date GetStart) {
        this.GetStart = GetStart;
    }

    /**
     * @return the GetEnd
     */
    public Date getGetEnd() {
        return GetEnd;
    }

    /**
     * @param GetEnd the GetEnd to set
     */
    public void setGetEnd(Date GetEnd) {
        this.GetEnd = GetEnd;
    }

    /**
     * @return the childCapacity
     */
    public Integer getChildCapacity() {
        return childCapacity;
    }

    /**
     * @param childCapacity the childCapacity to set
     */
    public void setChildCapacity(Integer childCapacity) {
        this.childCapacity = childCapacity;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.getDay());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof ConfigurationTimes)) {
            return false;
        }
        final ConfigurationTimes other = (ConfigurationTimes) obj;
        if (!Objects.equals(this.day, other.day)) {
            return false;
        }
        return true;
    }

}
