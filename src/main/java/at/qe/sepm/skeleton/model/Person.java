package at.qe.sepm.skeleton.model;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.domain.Persistable;

/**
 * Entity representing all basic information of a Person.
 */
@Entity
public abstract class Person implements Persistable<Long>, Comparable<Person> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long personId;

    private String firstName;

    private String lastName;

    private String email;

    private String photo;
    /* link to uploaded image */

    private Gender gender;
    /* change datatype */

    @Temporal(TemporalType.DATE)
    /*difference date timestamp*/
    private Date birthday;

    private String street;

    private String zipcode;

    private String city;

    private String phonePrivate;

    private String phoneBusiness;

    @OneToOne
    private User user;

    @ManyToOne(optional = true)
    private Person createUser;
    @Column(nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @ManyToOne(optional = true)
    private Person updateUser;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    /*private String firstName;
    private String lastName;*/
    boolean enabled;

    @ElementCollection(targetClass = UserRole.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "Person_UserRole")
    @Enumerated(EnumType.STRING)
    private Set<UserRole> roles;

    /**
     * @param personID the personID to set
     */
    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    /**
     * @return the personId
     */
    public Long getPersonId() {
        return personId;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Set<UserRole> getRoles() {
        return roles;
    }

    public void setRoles(Set<UserRole> roles) {
        this.roles = roles;
    }

    public Person getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Person createUser) {
        this.createUser = createUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Person getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Person updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return the photo
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * @param photo the photo to set
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    /**
     * @return the gender
     */
    public Gender getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(Gender gender) {
        this.gender = gender;
    }

    /**
     * @return the birthday
     */
    public Date getBirthday() {
        return birthday;
    }

    /**
     * @param birthday the birthday to set
     */
    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return the zipcode
     */
    public String getZipcode() {
        return zipcode;
    }

    /**
     * @param zipcode the zipcode to set
     */
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the phonePrivate
     */
    public String getPhonePrivate() {
        return phonePrivate;
    }

    /**
     * @param phonePrivate the phonePrivate to set
     */
    public void setPhonePrivate(String phonePrivate) {
        this.phonePrivate = phonePrivate;
    }

    /**
     * @return the phoneBusiness
     */
    public String getPhoneBusiness() {
        return phoneBusiness;
    }

    /**
     * @param phoneBusiness the phoneBusiness to set
     */
    public void setPhoneBusiness(String phoneBusiness) {
        this.phoneBusiness = phoneBusiness;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    public boolean isAdmin() {
        return getUser().getUsername().equals("admin");
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.getPersonId());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Person)) {
            return false;
        }
        final Person other = (Person) obj;
        if (!Objects.equals(this.personId, other.personId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.firstName + " " + this.lastName;
    }

    @Override
    public Long getId() {
        return this.personId;
    }

    @Override
    public boolean isNew() {
        return (null == createDate);
    }

    @Override
    public int compareTo(Person o) {
        if (o == null) {
            return 1;
        }
        return this.getId().compareTo(o.getId());
    }

    public int getAge(Date refdate) {
        Calendar bday = Calendar.getInstance();
        Calendar refday = Calendar.getInstance();
        bday.setTime(this.birthday);
        refday.setTime(refdate);
        return refday.get(Calendar.YEAR) - bday.get(Calendar.YEAR);
    }

}
