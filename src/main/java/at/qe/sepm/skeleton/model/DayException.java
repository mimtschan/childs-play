package at.qe.sepm.skeleton.model;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.data.domain.Persistable;

/**
 * Entity representing Exception a parent creates for a day for a child
 */
@Entity
public class DayException implements Persistable<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long exceptionId;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date date;

    private boolean absent;

    @Temporal(TemporalType.TIME)
    private Date alternateBringTime;

    @Temporal(TemporalType.TIME)
    private Date alternateGetTime;

    private String description;

    @ManyToOne
    @NotNull
    private Child child;

    @ManyToOne
    private ReferencePerson alternateGetter;

    private boolean confirmed;

    @ManyToOne(optional = true)
    private Person confirmedBy;

    @ManyToOne(optional = true)
    private Person createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.getExceptionId());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof DayException)) {
            return false;
        }
        final DayException other = (DayException) obj;
        if (!Objects.equals(this.exceptionId, other.exceptionId)) {
            return false;
        }
        return true;
    }

    @Override
    public boolean isNew() {
        return (getCreateDate() == null);
    }

    @Override
    public Long getId() {
        return exceptionId;
    }

    /**
     * @return the exceptionId
     */
    public Long getExceptionId() {
        return exceptionId;
    }

    /**
     * @param exceptionId the exceptionId to set
     */
    public void setExceptionId(Long exceptionId) {
        this.exceptionId = exceptionId;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @param absent the absent to set
     */
    public void setAbsent(boolean absent) {
        this.absent = absent;
    }

    /**
     * @return the alternateBringTime
     */
    public Date getAlternateBringTime() {
        return alternateBringTime;
    }

    /**
     * @param alternateBringTime the alternateBringTime to set
     */
    public void setAlternateBringTime(Date alternateBringTime) {
        this.alternateBringTime = alternateBringTime;
    }

    /**
     * @return the alternateGetTime
     */
    public Date getAlternateGetTime() {
        return alternateGetTime;
    }

    /**
     * @param alternateGetTime the alternateGetTime to set
     */
    public void setAlternateGetTime(Date alternateGetTime) {
        this.alternateGetTime = alternateGetTime;
    }

    /**
     * @return the child
     */
    public Child getChild() {
        return child;
    }

    /**
     * @param child the child to set
     */
    public void setChild(Child child) {
        this.child = child;
    }

    /**
     * @return the alternateGetter
     */
    public ReferencePerson getAlternateGetter() {
        return alternateGetter;
    }

    /**
     * @param alternateGetter the alternateGetter to set
     */
    public void setAlternateGetter(ReferencePerson alternateGetter) {
        this.alternateGetter = alternateGetter;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @param confirmed the confirmed to set
     */
    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    /**
     * @return the confirmedBy
     */
    public Person getConfirmedBy() {
        return confirmedBy;
    }

    /**
     * @param confirmedBy the confirmedBy to set
     */
    public void setConfirmedBy(Person confirmedBy) {
        this.confirmedBy = confirmedBy;
    }

    /**
     * @return the createdBy
     */
    public Person getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(Person createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the absent
     */
    public boolean isAbsent() {
        return absent;
    }

    /**
     * @return the confirmed
     */
    public boolean isConfirmed() {
        return confirmed;
    }

    /**
     * @return the createDate
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the updateDate
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
