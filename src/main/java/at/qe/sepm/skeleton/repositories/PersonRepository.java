package at.qe.sepm.skeleton.repositories;

import java.util.List;

import at.qe.sepm.skeleton.model.Person;

/**
 * Basic Repository for any person bases entity
 *
 * @author tade
 * @param <T> specific person entity
 */
public interface PersonRepository<T extends Person> extends AbstractRepository<T, Long> {

    List<T> findAllByEnabledIsTrue();

    List<T> findAllByEnabledIsFalse();

    List<T> findAllByCreateUser(Person person);

    List<T> findAllByCreateUserAndEnabledIsTrue(Person person);

}
