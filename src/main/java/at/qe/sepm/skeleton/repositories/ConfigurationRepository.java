package at.qe.sepm.skeleton.repositories;

import at.qe.sepm.skeleton.model.Configuration;

/**
 * Repository for Configuration
 *
 * @author tade
 */
public interface ConfigurationRepository extends AbstractRepository<Configuration, Long> {

}
