package at.qe.sepm.skeleton.repositories;

import at.qe.sepm.skeleton.model.Parent;

/**
 * Repository for parents
 *
 * @author tade
 */
public interface ParentRepository extends PersonRepository<Parent> {

}
