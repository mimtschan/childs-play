package at.qe.sepm.skeleton.repositories;

import java.util.Date;
import java.util.List;
import at.qe.sepm.skeleton.model.Child;

import at.qe.sepm.skeleton.model.DayException;

/**
 * Repository for Exceptions of Children
 *
 * @author tade
 */
public interface DayExceptionRepository extends AbstractRepository<DayException, Long> {

    List<DayException> findAllByConfirmedIsFalse();

    List<DayException> findAllByDateAndConfirmedIsTrue(Date date);

    DayException findFirstByChildAndDate(Child child, Date date);

    List<DayException> findAllByChild(Child child);
}
