package at.qe.sepm.skeleton.repositories;

import at.qe.sepm.skeleton.model.ConfigurationTimes;
import at.qe.sepm.skeleton.model.Weekday;

/**
 * Repository for Weekday specific configurations
 *
 * @author tade
 */
public interface ConfigurationTimesRepository extends AbstractRepository<ConfigurationTimes, Weekday> {

}
