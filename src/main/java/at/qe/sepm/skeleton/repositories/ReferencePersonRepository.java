package at.qe.sepm.skeleton.repositories;

import at.qe.sepm.skeleton.model.ReferencePerson;

/**
 * Repository for Reference Persons
 *
 * @author tade
 */
public interface ReferencePersonRepository extends PersonRepository<ReferencePerson> {

}
