package at.qe.sepm.skeleton.repositories;

import java.util.List;

import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.ChildReferencePersonRelation;
import at.qe.sepm.skeleton.model.ReferencePerson;

/**
 * Repository for Child and Reference Person Relations
 *
 * @author tade
 */
public interface ChildRefPersonRelationRepository extends AbstractRepository<ChildReferencePersonRelation, Long> {

    public List<ChildReferencePersonRelation> findByChild(Child child);

    public List<ChildReferencePersonRelation> findByReferencePerson(ReferencePerson referencePerson);

}
