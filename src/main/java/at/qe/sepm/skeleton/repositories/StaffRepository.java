package at.qe.sepm.skeleton.repositories;

import at.qe.sepm.skeleton.model.Staff;

/**
 * Repository for staff members
 *
 * @author tade
 */
public interface StaffRepository extends PersonRepository<Staff> {

}
