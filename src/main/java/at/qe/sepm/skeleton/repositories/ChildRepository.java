package at.qe.sepm.skeleton.repositories;

import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.Weekday;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository for Children
 *
 * @author tade
 */
public interface ChildRepository extends PersonRepository<Child> {

    @Query("SELECT u FROM Child u WHERE :weekday MEMBER OF u.subDates AND enabled = True")
    List<Child> findByWeekdayAndEnabled(@Param("weekday") Weekday weekday);

}
