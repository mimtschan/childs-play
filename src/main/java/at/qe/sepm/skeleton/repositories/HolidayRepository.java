package at.qe.sepm.skeleton.repositories;

import java.util.Date;

import at.qe.sepm.skeleton.model.Holiday;

/**
 * Repository for holidays
 *
 * @author tade
 */
public interface HolidayRepository extends AbstractRepository<Holiday, Date> {

}
