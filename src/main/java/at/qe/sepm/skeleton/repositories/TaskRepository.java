package at.qe.sepm.skeleton.repositories;

import java.util.List;

import at.qe.sepm.skeleton.model.Parent;
import at.qe.sepm.skeleton.model.Task;

/**
 * Repository for taks
 *
 * @author tade
 */
public interface TaskRepository extends AbstractRepository<Task, Long> {

    List<Task> findAllByDoneIsNull();

    List<Task> findAllByAssignedAndDoneIsNull(Parent assigned);

}
